<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="en">
<context>
    <name>MKVExtractorQt</name>
    <message>
        <location filename="MKVExtractorQt5.py" line="549"/>
        <source>Error : Not enough space available (need 2 x MKV size)</source>
        <translation type="obsolete">Erreur : Pas assez de place disponible (besoin de 2 x la taille du MKV)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="753"/>
        <source>Select the output folder</source>
        <translation type="obsolete">Sélectionner le dossier de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="869"/>
        <source>This track is a {} type.</source>
        <translation type="obsolete">Cette piste est de type {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="548"/>
        <source>Error : No License file found</source>
        <translation type="obsolete">Erreur : Pas de fichier Licence trouvé</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="752"/>
        <source>Select the output MKV file</source>
        <translation type="obsolete">Séléctionner le fichier MKV de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="746"/>
        <source>Selected file: {}.</source>
        <translation type="obsolete">Fichier sélectionné : {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="747"/>
        <source>Selected folder: {}.</source>
        <translation type="obsolete">Dossier sélectionné : {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="740"/>
        <source>New value for &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; option: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Nouvelle valeur pour l&apos;option &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="779"/>
        <source>Command execution: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Éxécution de la commande : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="839"/>
        <source>Selected file: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Fichier sélectionné : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="841"/>
        <source>Selected folder: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Dossier sélectionné : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="867"/>
        <source>This track can be renamed.</source>
        <translation type="obsolete">Cette piste peut être renommée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1076"/>
        <source>No information.</source>
        <translation type="obsolete">Pas d&apos;information.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="769"/>
        <source>Work with track number {}.</source>
        <translation type="obsolete">Travailler avec la piste numéro {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="778"/>
        <source> All commands were canceled </source>
        <translation type="obsolete"> Toutes les commandes ont été annulées </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="780"/>
        <source> The last command returned an error </source>
        <translation type="obsolete"> La dernière commande a renvoyé une erreur </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="860"/>
        <source>This track can be renamed and must contain an extension to avoid reading errors.</source>
        <translation type="obsolete">Cette piste peut être renommée et doit contenir une extension pour eviter les erreurs de lecture.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="916"/>
        <source> Paused software time to read the subtitles </source>
        <translation type="obsolete"> Mise en pause du logiciel le temps de lire les sous-titres </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="768"/>
        <source>chapters</source>
        <translation type="obsolete">chapitres</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="773"/>
        <source>tags</source>
        <translation type="obsolete">tags</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="871"/>
        <source>The subtitle language is not avaible in Tesseract list langs: {}</source>
        <translation type="obsolete">La langue du sous-titre n&apos;est pas disponible dans la liste des langues de Tesseract :{} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="911"/>
        <source>All subtitles images could not be recognized.
We must therefore specify the missing texts.</source>
        <translation type="obsolete">Tous les sous-titres des images n&apos;ont pas pu être reconnus.
Il faut donc préciser les textes manquants.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1055"/>
        <source>Subtitle converter to images.</source>
        <translation type="obsolete">Convertion des sous titres en images.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="730"/>
        <source>No power change.</source>
        <translation type="obsolete">Pas de modification de la puissance.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="751"/>
        <source>Select the input MKV File</source>
        <translation type="obsolete">Sélectionner le fichier MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="638"/>
        <source>Subtitle Bitmap.</source>
        <translation type="obsolete">Sous-titre Bitmap.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="639"/>
        <source>The SubRip file format is perhaps the most basic of all subtitle formats.</source>
        <translation type="obsolete">Le format SRT est sûrement le format de sous-titre le plus simple.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="640"/>
        <source>Vob Subtitle.</source>
        <translation type="obsolete">Sous-titre vob.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="641"/>
        <source>SubStation Alpha is a subtitle file format created by CS Low that allows for more advanced subtitles than SRT format.</source>
        <translation type="obsolete">SubStation Alpha est un format de sous-titre créé par CS Low qui permet des effets plus avancées que le format SRT.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="642"/>
        <source>MP3 is an encoding format for digital audio which uses a form of lossy data compression.</source>
        <translation type="obsolete">Le format MP3 est un encodeur avec perte pour l&apos;audio digitale qui utilise une forme de perte de donnée à la compression.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="643"/>
        <source>Ogg is a free, open container format maintained by the Xiph.Org Foundation.</source>
        <translation type="obsolete">Ogg est libre, un contenaire ouvert maintenu par la fondation Xiph.Org.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="644"/>
        <source>DTS is a series of multichannel audio technologies owned by DTS, Inc.</source>
        <translation type="obsolete">DTS est une serie d&apos;audio multicanaux dévellopé par DTS, Inc.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="645"/>
        <source>Advanced Audio Coding is a standardized, lossy compression and encoding scheme for digital audio.</source>
        <translation type="obsolete">Advanced Audio Coding est un standard de compression avec perte et un système d&apos;encodage pour l&apos;audio digitale. </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="646"/>
        <source>Dolby Digital audio codec, the audio compression is lossy.</source>
        <translation type="obsolete">Codec audio Dolby Digital, une compression audio avec perte.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="647"/>
        <source>The cook codec is a lossy audio compression codec developed by RealNetworks.</source>
        <translation type="obsolete">Le codec cook est un format compressé avec perte développé par RealNetworks.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="648"/>
        <source>RealVideo 4.0, suspected to be based on H.264. RV40 is RealNetworks&apos; proprietary codec.</source>
        <translation type="obsolete">Le RealVideo 4.0 est supposé être basé sur le codec H264. Le RV40 est un codec propriétaire de RealNetwork&apos;. </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="649"/>
        <source>H264 is a video compression format, and is currently one of the most commonly used formats for the recording, compression, and distribution of video content.</source>
        <translation type="obsolete">Le H264 est l&apos;un des formats de compression vidéo les plus utilisé actuellement pour l&apos;enregistrement, la compression et la distribution de vidéo.</translation>
    </message>
    <message numerus="yes">
        <location filename="MKVExtractorQt5.py" line="1929"/>
        <source>%n image(s)</source>
        <translation type="obsolete">
            <numerusform>%n image</numerusform>
            <numerusform>%n images</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="MKVExtractorQt5.py" line="1929"/>
        <source>on %n image(s)</source>
        <translation type="obsolete">
            <numerusform>sur %n image</numerusform>
            <numerusform>sur %n images</numerusform>
        </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="727"/>
        <source>The temporary files are the extracted tracks.</source>
        <translation type="obsolete">Les fichiers temporaires sont les pistes extraites.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="731"/>
        <source>Multiplying audio power by {}.</source>
        <translation type="obsolete">Multiplie la puissance audio par {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="734"/>
        <source>Convert the audio quality in {} kbits/s.</source>
        <translation type="obsolete">Convertit la qualité audio en {} kbits/s.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="737"/>
        <source>The audio will not use the same number of channels, the audio will be stereo (2 channels).</source>
        <translation type="obsolete">L&apos;audio ne conservera pas son nombre de canal, l&apos;audio sera en stéréo (2 canaux).</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="739"/>
        <source>Auto opening of subtitle srt files for correction. The software will be paused.</source>
        <translation type="obsolete">Ouvre automatiquement les fichiers sous-titres srt pour les corriger. Le logiciel sera mis en pause.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="910"/>
        <source>SRT subtitle creation.</source>
        <translation type="obsolete">Création du fichier sous-titre SRT.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="766"/>
        <source>Change the language if it&apos;s not right. &apos;und&apos; means &apos;Undetermined&apos;.</source>
        <translation type="obsolete">Changer la langue si ce n&apos;est pas la bonne. &apos;und&apos; signifie &apos;Indéterminé&apos;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="862"/>
        <source>This track can be displayed via an icon click.</source>
        <translation type="obsolete">Cette piste peut être visualisée via un clique sur l&apos;icône.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="601"/>
        <source>Choose the good number of frames per second if needed. Useful in case of audio lag. Normal : 23.976, 25.000 and 30.000.</source>
        <translation type="obsolete">Choisir le bon nombre d&apos;images par seconde si besoin. Utile en cas de décalage avec l&apos;audio.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source> {} execution is finished </source>
        <translation type="obsolete"> L&apos;éxécution de {} est términée </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="783"/>
        <source> {} execution in progress </source>
        <translation type="obsolete"> L&apos;éxécution de {} est en cours </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="726"/>
        <source>Delete temporary files</source>
        <translation type="obsolete">Supprimer les fichiers temporaires de la ré-encapsulation</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="729"/>
        <source>Increase the sound power</source>
        <translation type="obsolete">Amplifier la puissance audio</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="732"/>
        <source>Power x {}</source>
        <translation type="obsolete">Puissance x {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="733"/>
        <source>List of available flow rates of conversion</source>
        <translation type="obsolete">Liste des débits de conversion disponibles</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="735"/>
        <source>{} kbits/s</source>
        <translation type="obsolete">{} kbits/s</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="736"/>
        <source>Switch to stereo during conversion</source>
        <translation type="obsolete">Passer en stéréo lors de la conversion</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="738"/>
        <source>Opening subtitles before encapsulation</source>
        <translation type="obsolete">Ouvrir les sous-titres avant la ré-encapsulation</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="765"/>
        <source>If the remuxed file has reading problems, change this value.</source>
        <translation type="obsolete">Si le fichier ré-encapsulé à des problèmes de lecture, changer cette valeur.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="770"/>
        <source>Work with attachment number {}.</source>
        <translation type="obsolete">Travailler sur le fichier joint n° {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="771"/>
        <source>Work with {}.</source>
        <translation type="obsolete">Travailler sur les {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="546"/>
        <source>Error: The {} file given as argument does not exist.</source>
        <translation type="obsolete">Erreur : Le fichier {} donné en argument n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="547"/>
        <source>Error: Too many arguments given: {}</source>
        <translation type="obsolete">Erreur : Trop d&apos;arguments donnés : {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="863"/>
        <source>This track can not be displayed.</source>
        <translation type="obsolete">Cette piste ne peut pas être visualisée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="782"/>
        <source> MKV File Tracks </source>
        <translation type="obsolete"> Pistes du fichier MKV </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="728"/>
        <source>Use FFMpeg for the conversion.</source>
        <translation type="obsolete">Utiliser FFMpeg pour la conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="644"/>
        <source>About MKV Extractor Gui</source>
        <translation type="obsolete">À propos de MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="543"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v5.1.1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded Bash.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;Creative Commons BY-NC-SA&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;A big thank to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Create by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v5.1.1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI d&apos;extraction, modification et de ré-encapsulage des pistes d&apos;un fichier matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Ce logiciel fait suite à plusieurs autres qui étaient codés en Bash.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Ce logiciel est sous licence &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Un grand merci aux forums python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; pour leur patience.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), Novembre 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="691"/>
        <source>Help me!</source>
        <translation type="obsolete">À l&apos;aide !</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="554"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for the interest you have shown in this software.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vous êtes perdu ? Vous avez besoin d&apos;aide ?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normalement toutes les informations nécessaires sont présentes:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Lisez les informations de la barre d&apos;état lors du déplacement de la souris sur les widgets&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Malgré tout, si vous avez besoin de plus amples informations:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Mon adresse mail : &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Merci de l&apos;intérêt que vous portez à ce logiciel.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="876"/>
        <source>Edition of files who have the md5: {}</source>
        <translation type="obsolete">Édition des fichiers ayant le md5 : {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="679"/>
        <source>Wrong arguments</source>
        <translation type="obsolete">Mauvais arguments</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="680"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; file given as argument does not exist.</source>
        <translation type="obsolete">Le fichier &lt;b&gt;{}&lt;/b&gt; donné en argument n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="811"/>
        <source>&lt;b&gt;Too many arguments given:&lt;/b&gt; {}</source>
        <translation type="obsolete">&lt;b&gt;Trop d&apos;arguments donnés :&lt;/b&gt; {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="687"/>
        <source>Space available</source>
        <translation type="obsolete">Espace disponible</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="549"/>
        <source>Not enough space available (need 2 x MKV size)</source>
        <translation type="obsolete">Pas assez de place disponible (necessite 2 fois plus que la taille du MKV)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="870"/>
        <source>Tesseract langs error</source>
        <translation type="obsolete">Erreur de langue avec Tesseract</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="877"/>
        <source>Image progression : {} on {}</source>
        <translation type="obsolete">Progression des images : {} sur {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source>Not enough space available.

It is advisable to have at least twice the size of free space on the disk file.</source>
        <translation type="obsolete">Erreur : Pas assez de place disponible.

Il est conseillé d&apos;avoir au moins deux fois la taille du fichier d&apos;espace libre sur le disque.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1023"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vous êtes perdu ? Vous avez besoin d&apos;aide ?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normalement toutes les informations nécessaires sont présentes:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Lisez les informations de la barre d&apos;état lors du déplacement de la souris sur les widgets&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Malgré tout, si vous avez besoin de plus amples informations:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Mon adresse mail : &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Merci de l&apos;intérêt que vous portez à ce logiciel.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="776"/>
        <source>Change the fps value if needed. Useful in case of audio lag. Normal : 23.976, 25.000 and 30.000.</source>
        <translation type="obsolete">Changer le nombre d&apos;images par seconde si besoin. Utile en cas de décalage avec l&apos;audio.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="725"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI d&apos;extraction, modification et de ré-encapsulage des pistes d&apos;un fichier matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Ce logiciel fait suite à plusieurs autres qui étaient codés en Bash.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Ce logiciel est sous licence &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Un grand merci aux forums python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; pour leur patience.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), Novembre 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="663"/>
        <source>This file is not supported by mkvmerge.
Do you want convert this file in mkv ?</source>
        <translation type="obsolete">Ce fichier n&apos;est pas supporté par mkvmerge.
Voulez-vous le convertir en mkv ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="951"/>
        <source>Not supported file</source>
        <translation type="obsolete">Fichier non compatible</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="755"/>
        <source>MKV Merge Gui or MKV Extractor Qt ?</source>
        <translation type="obsolete">MKV Merge Gui ou MKV Extractor Qt ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="768"/>
        <source>You want extract and reencapsulate the tracks but without use other options.

If you just need to make this, you should use MMG (mkvmerge gui) who is more adapted for this job.

What software do you want use ?</source>
        <translation type="obsolete">Vous voulez extraire et réencapsuler des pistes sans autres options.

Si vous voulez juste faire ça, vous devriez utiliser MMG (MKV Merge Gui) qui est plus adapté à ce travail.

Quel logiciel voulez-vous utiliser ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1069"/>
        <source>You want extract and reencapsulate the tracks without use other options.

If you just need to make this, you should use MMG (MKV Merge gui) who is more adapted for this job.

What software do you want use ?</source>
        <translation type="obsolete">Vous voulez extraire et réencapsuler des pistes sans autres options.

Si vous voulez juste faire ça, vous devriez utiliser MMG (MKV Merge Gui) qui est plus adapté à ce travail.

Quel logiciel voulez-vous utiliser ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1053"/>
        <source>Select the output folder to use for convert the file</source>
        <translation type="obsolete">Sélectionner le dossier de sortie à utiliser pour convertir le fichier</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash and it codec in python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI d&apos;extraction, modification et de ré-encapsulage des pistes d&apos;un fichier matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Ce logiciel fait suite à plusieurs autres qui étaient codés en Bash et est codé en python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Ce logiciel est sous licence &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Un grand merci aux forums python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; pour leur patience.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), Novembre 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="761"/>
        <source>audio</source>
        <translation type="obsolete">audio</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="762"/>
        <source>subtitles</source>
        <translation type="obsolete">sous-titres</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="763"/>
        <source>video</source>
        <translation type="obsolete">vidéo</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="661"/>
        <source>Do not ask again</source>
        <translation type="obsolete">Ne plus me demander</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="664"/>
        <source>MKVMerge Warning</source>
        <translation type="obsolete">Alerte MKVMerge</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="665"/>
        <source>A warning has occurred during the convertion of the file, read the feedback informations.</source>
        <translation type="obsolete">Une alarme est survenue pendant la conversion du fichier, lisez les retours.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="666"/>
        <source>Do not warn me</source>
        <translation type="obsolete">Ne plus m&apos;avertir</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="667"/>
        <source>Choose the out folder of the new mkv file</source>
        <translation type="obsolete">Choix du dossier de sorti du nouveau fichier mkv</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1097"/>
        <source>Always ask me</source>
        <translation type="obsolete">Toujours me demander</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1098"/>
        <source>Always use the same folder as the not supported file</source>
        <translation type="obsolete">Toujours utiliser le même dossier que le fichier non supporté</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="763"/>
        <source>Always use this folder</source>
        <translation type="obsolete">Toujours utiliser ce dossier</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="662"/>
        <source>File needs to be converted</source>
        <translation type="obsolete">Le fichier doit être converti</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="745"/>
        <source>Use the right click for see options.</source>
        <translation type="obsolete">Utiliser le clic droit pour voir les options.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1048"/>
        <source>Use the same output folder as the MKV file</source>
        <translation type="obsolete">Utiliser le même dossier de sortie que le fichier MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="849"/>
        <source>Keep in memory my choice</source>
        <translation type="obsolete">Conserver mon choix en mémoire</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="756"/>
        <source>You want extract and reencapsulate the tracks without use other options.

If you just need to make this, you should use MMG (MKV Merge gui) who is more adapted for this job.

What software do you want use ?
</source>
        <translation type="obsolete">Vous voulez extraire et réencapsuler des pistes sans autres options.

Si vous voulez juste faire ça, vous devriez utiliser MMG (MKV Merge Gui) qui est plus adapté à ce travail.

Quel logiciel voulez-vous utiliser ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="657"/>
        <source>All compatible Files</source>
        <translation type="obsolete">Tous les Fichiers compatibles</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="658"/>
        <source>Matroska Files</source>
        <translation type="obsolete">Fichiers Matroska</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="659"/>
        <source>Other Files</source>
        <translation type="obsolete">Autres Fichiers</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="692"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=1508741&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vous êtes perdu ? Vous avez besoin d&apos;aide ?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normalement toutes les informations nécessaires sont présentes:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Lisez les informations de la barre d&apos;état lors du déplacement de la souris sur les widgets&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Malgré tout, si vous avez besoin de plus amples informations:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Mon adresse mail : &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Merci de l&apos;intérêt que vous portez à ce logiciel.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="695"/>
        <source>Quality of the ac3 file converted.</source>
        <translation type="obsolete">Qualité du fichier ac3 converti.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="696"/>
        <source>Power of the ac3 file converted.</source>
        <translation type="obsolete">Puissance du fichier ac3 converti.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="699"/>
        <source>Delete temporary files.</source>
        <translation type="obsolete">Supprimer les fichiers temporaires.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="701"/>
        <source>Show or hide the information feedback box.</source>
        <translation type="obsolete">Afficher ou cacher la box de retour d&apos;informations.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="702"/>
        <source>Anchor or loose information feedback box.</source>
        <translation type="obsolete">Ancrer ou perdre la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="706"/>
        <source>Software to use for just encapsulate.</source>
        <translation type="obsolete">Logiciel à utiliser pour simplement encapsuler.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="707"/>
        <source>Skip the proposal to softaware to use.</source>
        <translation type="obsolete">Sauter la proposition du logiciel à utiliser.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="708"/>
        <source>Skip the confirmation of the conversion.</source>
        <translation type="obsolete">Sauter la confirmation de la conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="709"/>
        <source>Hide the information of the conversion warning.</source>
        <translation type="obsolete">Cacher le retour d&apos;alarmes de conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="899"/>
        <source>Always use this folder as output folder for the files converted.</source>
        <translation type="obsolete">Toujours utiliser ce dossier comme emplacement pour les fichiers convertis.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="900"/>
        <source>Use the same input and output folder for the files converted.</source>
        <translation type="obsolete">Utiliser le même dossier en entrée et sortie pour les fichiers convertis.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="710"/>
        <source>Folder of the MKV files.</source>
        <translation type="obsolete">Dossier des fichiers MKV.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="711"/>
        <source>Output folder for the new MKV files.</source>
        <translation type="obsolete">Dossier de sorti pour les nouveaux fichiers MKV.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="712"/>
        <source>Software language to use.</source>
        <translation type="obsolete">Langue à utiliser pour le logiciel.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="713"/>
        <source>Remove the Qt file who keeps the list of the recent files for the window selection.</source>
        <translation type="obsolete">Supprime le fichier Qt qui conserve la liste des fichiers récents de la fenêtre de selection.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="714"/>
        <source>Use the same input and output folder.</source>
        <translation type="obsolete">Utiliser le même dossier en entrée et sortie.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="716"/>
        <source>Switch to stereo during conversion.</source>
        <translation type="obsolete">Passer en stéréo lors de la conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="717"/>
        <source>Opening subtitles before encapsulation.</source>
        <translation type="obsolete">Ouvrir les sous-titres avant la ré-encapsulation.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="720"/>
        <source>Keep in memory the aspect and the position of the window for the next opened.</source>
        <translation type="obsolete">Conserver en mémoire l&apos;aspect et la position de la fenêtre pour le prochain lancement.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="750"/>
        <source>Keep in memory the last file opened for open it at the next launch of MKV Extractor Qt (to use for tests)</source>
        <translation type="obsolete">Conserver en mémoire le dernier fichier ouvert pour le prochain lancement de MKV Extractor Qt (util pour tester)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="677"/>
        <source>The last file doesn&apos;t exist</source>
        <translation type="obsolete">Le dernier fichier n&apos;existe plus</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="678"/>
        <source>You have checked the option who reload the last file to the launch of MKV Extractor Qt, but this last file doesn&apos;t exist anymore.</source>
        <translation type="obsolete">Vous avez coché l&apos;option qui recharge le dernier fichier au lancement de MKV Extractor Qt, mais ce fichier n&apos;existe plus.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="698"/>
        <source>View more informations in feedback box.</source>
        <translation type="obsolete">Afficher plus d&apos;informations dans la box de retours.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="751"/>
        <source>Always use the same output folder as the input MKV file</source>
        <translation type="obsolete">Toujours utiliser le même dossier de sortie que celui d&apos;entrée des fichiers MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="752"/>
        <source>Always use the default file rename (Clean_FileName) and doesn&apos;t ask me</source>
        <translation type="obsolete">Toujours utiliser le renommage par défaut (Clean_NomDuFichier) et ne plus demander</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="761"/>
        <source>Always ask me the output folder</source>
        <translation type="obsolete">Toujours me demander le dossier de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="762"/>
        <source>Always use the same folder as the not supported input file</source>
        <translation type="obsolete">Toujours utiliser le même dossier que celui des fichiers non supportés</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="682"/>
        <source>Wrong value</source>
        <translation type="obsolete">Valeur erronée</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="683"/>
        <source>Wrong value for the &lt;b&gt;{}&lt;/b&gt; option, MKV Extractor Qt will use the default value.</source>
        <translation type="obsolete">Valeur erronée pour l&apos;option &lt;b&gt;{}&lt;/b&gt;, MKV Extractor Qt va utiliser la valeur par défaut.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="684"/>
        <source>Wrong path for the &lt;b&gt;{}&lt;/b&gt; option, MKV Extractor Qt will use the default path.</source>
        <translation type="obsolete">Emplacement erroné pour l&apos;option &lt;b&gt;{}&lt;/b&gt;, MKV Extractor Qt va utiliser l&apos;emplacement par défaut.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="685"/>
        <source>No way to open this file</source>
        <translation type="obsolete">Pas moyen d&apos;ouvrir ce fichier</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="686"/>
        <source>The file to open contains quotes (&quot;) in its name. It&apos;s impossible to open a file with this carac. Please rename it.</source>
        <translation type="obsolete">Le fichier à ouvrir contient des guillemets (&quot;) dans son nom, il est impossible d&apos;ouvrir un fichier avec ce caractére. Merci de le renommer.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="700"/>
        <source>Remove the error message if the last file doesn&apos;t exist.</source>
        <translation type="obsolete">Supprimer ce message d&apos;erreur si le dernier fichier n&apos;existe plus.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="705"/>
        <source>Keep in memory the last file opened for open it at the next launch of MKV Extractor Qt.</source>
        <translation type="obsolete">Conserver en mémoire le dernier fichier ouvert pour le prochain lancement de MKV Extractor Qt.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="892"/>
        <source>Automatically rename the output file name in Clean_FileName.</source>
        <translation type="obsolete">Renommer automatiquement le fichier de sortie en CLean_NomDuFichier.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="893"/>
        <source>Use the same input and output folder for the files cleaned.</source>
        <translation type="obsolete">Utiliser le même dossier en entrée et sortie pour les fichiers nettoyés.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="894"/>
        <source>Always use this folder as output folder for the files cleaned.</source>
        <translation type="obsolete">Toujours utiliser ce dossier comme sortie des fichiers nettoyés.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="715"/>
        <source>Automatically rename the output file name in MEG_FileName.</source>
        <translation type="obsolete">Renommer automatiquement le fichier de sortie en MEG_NomDuFichier.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="748"/>
        <source>Always use the same output folder as the input MKV file (automatically updated)</source>
        <translation type="obsolete">Toujours utiliser le même dossier de sortie que celui d&apos;entré du fichier MKV (mis à jour automatiquement)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="758"/>
        <source>Always use the default file rename (MEG_FileName)</source>
        <translation type="obsolete">Toujours utiliser le renommage par défaut (MEG_NomDuFichier)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="759"/>
        <source>Choose the output file name</source>
        <translation type="obsolete">Choisir le dossier de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="672"/>
        <source>Awaiting resume</source>
        <translation type="obsolete">Reprise en attente</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="746"/>
        <source>The software is pausing.
Thanks to clic on the &apos;Resume work&apos; button or &apos;Cancel work&apos; for cancel all the work and remove the temporary files.</source>
        <translation type="obsolete">Le logiciel est en pause.
Merci de cliquer sur le bouton &apos;Reprise du travail&apos; ou &apos;Annulation du travail&apos; pour annuler tout le travail et supprimer les fichiers temporaires.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="674"/>
        <source>Resume work</source>
        <translation type="obsolete">Reprise du travail</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="675"/>
        <source>Cancel work</source>
        <translation type="obsolete">Annulation du travail</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="681"/>
        <source>&lt;b&gt;Too many arguments given:&lt;/b&gt;&lt;br/&gt; - {} </source>
        <translation type="obsolete">&lt;b&gt;Trop d&apos;arguments donnés :&lt;/b&gt;&lt;br/&gt; - {} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="786"/>
        <source>Convert DTS to AC3</source>
        <translation type="obsolete">Conversion en AC3</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="787"/>
        <source>Convert DTS tracks automatically to AC3, which is much more compatible.</source>
        <translation type="obsolete">Conversion automatique des pistes audio en AC3, format lisible par de nombreux instruments.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="741"/>
        <source>No change the quality</source>
        <translation type="obsolete">Ne pas modifier la qualité</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="804"/>
        <source>The quality of the audio track will not be changed.</source>
        <translation type="obsolete">La qualité de la piste audio ne sera pas modifiée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="724"/>
        <source>Convert in AC3</source>
        <translation type="obsolete">Convertion en AC3</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="725"/>
        <source>Convert audio tracks automatically to AC3.</source>
        <translation type="obsolete">Convertit automatiquement les pistes audio en AC3.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="742"/>
        <source>The quality of the audio tracks will not be changed.</source>
        <translation type="obsolete">La qualité de la piste audio ne sera pas modifiée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="743"/>
        <source>No change the power</source>
        <translation type="obsolete">Ne pas modifier la puissance</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="744"/>
        <source>The power of the audio tracks will not be changed.</source>
        <translation type="obsolete">La puissance de la piste audio ne sera pas modifiée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="673"/>
        <source>The software is &lt;b&gt;pausing&lt;/b&gt;.&lt;br/&gt;Thanks to clic on the &apos;&lt;b&gt;Resume work&lt;/b&gt;&apos; button or &apos;&lt;b&gt;Cancel work&lt;/b&gt;&apos; for cancel all the work and remove the temporary files.</source>
        <translation type="obsolete">Le logiciel est en &lt;b&gt;pause&lt;/b&gt;.&lt;br/&gt;Merci de cliquer sur le bouton &apos;&lt;b&gt;Reprise du travail&lt;/b&gt;&apos; ou &apos;&lt;b&gt;Annulation du travail&lt;/b&gt;&apos; pour annuler tout le travail et supprimer les fichiers temporaires.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="650"/>
        <source>Quit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="722"/>
        <source>Number of CPU to use</source>
        <translation type="obsolete">Nombre de CPU à utiliser</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="723"/>
        <source>Choose the number of CPU to use with Tesseract.</source>
        <translation type="obsolete">Choisir le nombre de CPU à utiliser avec Tesseract.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="718"/>
        <source>Display or hide the system tray icon.</source>
        <translation type="obsolete">Afficher ou cacher l&apos;icone de la zone de notification.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="719"/>
        <source>Number of CPU to use with Tesseract, by default: max value.</source>
        <translation type="obsolete">Nombre de CPU à utiliser avec Tesseract, par défaut : valeur max.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="651"/>
        <source>The command(s) have finished</source>
        <translation type="obsolete">Commande(s) terminée(s)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="652"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; command have finished its work.</source>
        <translation type="obsolete">La commande &lt;b&gt;{}&lt;/b&gt; a terminé son travail.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="653"/>
        <source>All commands have finished their work.</source>
        <translation type="obsolete">Toutes les commande ont terminé leur travail.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="767"/>
        <source>This track can be renamed and must contain an extension to avoid reading errors by doubleclicking.</source>
        <translation type="obsolete">Cette piste peut être renommée et doit contenir une extension pour eviter les erreurs de lecture via un double clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="772"/>
        <source>This track can be renamed by doubleclicking.</source>
        <translation type="obsolete">Cette piste peut être renommée via un double clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="774"/>
        <source>This track is a {} type and cannot be previewed.</source>
        <translation type="obsolete">Cette piste est de type {} et ne peut être prévisualisée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="688"/>
        <source>Not enough space available in the &lt;b&gt;{}&lt;/b&gt; folder.&lt;br/&gt;It is advisable to have at least twice the size of free space on the disk file.&lt;br&gt;Free disk space: &lt;b&gt;{}&lt;/b&gt;.&lt;br&gt;File size: &lt;b&gt;{}&lt;/b&gt;.</source>
        <translation type="obsolete">Erreur : Pas assez de place disponible dans le dossier &lt;b&gt;{}&lt;/b&gt;.&lt;br/&gt;Il est conseillé d&apos;avoir au moins deux fois la taille du fichier d&apos;espace libre sur le disque.&lt;br&gt;Espace disque libre : &lt;b&gt;{}&lt;/b&gt;.&lt;br&gt;Taille du fichier : &lt;b&gt;{}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="689"/>
        <source>Not enough space available in &lt;b&gt;{}&lt;/b&gt; folder.&lt;br/&gt;Free space in the disk: &lt;b&gt;{}&lt;/b&gt;&lt;br/&gt;File size: &lt;b&gt;{}&lt;/b&gt;.</source>
        <translation type="obsolete">Erreur : Pas assez de place disponible dans le dossier&lt;b&gt;{}&lt;/b&gt;.&lt;br/&gt;Espace disque libre : &lt;b&gt;{}&lt;/b&gt;&lt;br/&gt;Taille du fichier : &lt;b&gt;{}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="697"/>
        <source>Skip the free space disk test.</source>
        <translation type="obsolete">Saute la verification d&apos;espace libre.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="703"/>
        <source>The folder to use for extract temporaly the attachements file to view them.</source>
        <translation type="obsolete">Dossier à utiliser pour extraire temporairement les fichiers joints et les visualiser.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="870"/>
        <source>This attachment file is a {} type, he can be extracted (speedy) and viewed by clicking.</source>
        <translation type="obsolete">Ce fichier joint est de type {}, il peut être extrait (rapide) et visualisé via un clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="775"/>
        <source>This attachment file is a {} type, it can be extracted (speedy) and viewed by clicking.</source>
        <translation type="obsolete">Ce fichier joint est de type {}, il peut être extrait (rapide) et visualisé via un clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="669"/>
        <source>Already existing file</source>
        <translation type="obsolete">Fichier déjà existant</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="670"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; is already existing, overwrite it?</source>
        <translation type="obsolete">Le fichier &lt;b&gt;{}&lt;/b&gt; existe déjà, faut-il l&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="655"/>
        <source>Use the right click for view options.</source>
        <translation type="obsolete">Utiliser le clic droit pour voir les options.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="694"/>
        <source>Skip the existing file test.</source>
        <translation type="obsolete">Saute la verification d&apos;existence du fichier.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="645"/>
        <source>About MKVToolNix</source>
        <translation type="obsolete">À propos de MKVToolNix</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="646"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKVToolNix&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MKVToolNix is a set of tools to create, alter and inspect Matroska files under Linux, other Unices and Windows.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v2&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.Here, the last version of the &lt;a href=&quot;https://mkvtoolnix.download/downloads.html#ubuntu&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;mkvtoolnix&lt;/span&gt;&lt;/a&gt;.&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Moritz Bunkus&lt;/span&gt; &lt;moritz@bunkus.org&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKVToolNix&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MKVToolNix est un ensemble d&apos;outils pour créer, modifier et inspecter des fichiers Matroska sous Linux, autres Unix et Windows.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Ce logiciel est sous licence &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v2&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.Here, the last version of the &lt;a href=&quot;https://mkvtoolnix.download/downloads.html#ubuntu&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;mkvtoolnix&lt;/span&gt;&lt;/a&gt;.&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Moritz Bunkus&lt;/span&gt; &lt;moritz@bunkus.org&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="647"/>
        <source>They talk about MKV Extractor Gui</source>
        <translation type="obsolete">Ils parlent de MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="648"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://sysads.co.uk/2014/09/install-mkv-extractor-qt-5-1-4-ubuntu-14-04/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;sysads.co.uk&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.softpedia.com/reviews/linux/mkv-extractor-qt-review-496919.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linux.softpedia.com/get/Multimedia/Video/MKV-Extractor-Qt-103555.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;linux.softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://zenway.ru/page/mkv-extractor-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;zenway.ru&lt;/span&gt;&lt;/a&gt; (Russian)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linuxg.net/how-to-install-mkv-extractor-qt-5-1-4-on-ubuntu-14-04-linux-mint-17-elementary-os-0-3-deepin-2014-and-other-ubuntu-14-04-derivatives/&quot;&gt;linuxg.net&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://la-vache-libre.org/mkv-extractor-gui-virer-les-sous-titres-inutiles-de-vos-fichiers-mkv-et-plus-encore/&quot;&gt;la-vache-libre.org&lt;/span&gt;&lt;/a&gt; (French)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://passionexubuntu.altervista.org/index.php/it/kubuntu/1152-mkv-extractor-qt-vs-5-1-3-kde.html&quot;&gt;passionexubuntu.altervista.org&lt;/span&gt;&lt;/a&gt; (Italian)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://sysads.co.uk/2014/09/install-mkv-extractor-qt-5-1-4-ubuntu-14-04/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;sysads.co.uk&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.softpedia.com/reviews/linux/mkv-extractor-qt-review-496919.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;softpedia.com&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linux.softpedia.com/get/Multimedia/Video/MKV-Extractor-Qt-103555.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;linux.softpedia.com&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://zenway.ru/page/mkv-extractor-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;zenway.ru&lt;/span&gt;&lt;/a&gt; (Russe)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linuxg.net/how-to-install-mkv-extractor-qt-5-1-4-on-ubuntu-14-04-linux-mint-17-elementary-os-0-3-deepin-2014-and-other-ubuntu-14-04-derivatives/&quot;&gt;linuxg.net&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://la-vache-libre.org/mkv-extractor-gui-virer-les-sous-titres-inutiles-de-vos-fichiers-mkv-et-plus-encore/&quot;&gt;la-vache-libre.org&lt;/span&gt;&lt;/a&gt; (Fraçais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://passionexubuntu.altervista.org/index.php/it/kubuntu/1152-mkv-extractor-qt-vs-5-1-3-kde.html&quot;&gt;passionexubuntu.altervista.org&lt;/span&gt;&lt;/a&gt; (Italien)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>MKVExtractorQt5</name>
    <message>
        <location filename="MKVExtractorQt5.py" line="642"/>
        <source>About MKV Extractor Gui</source>
        <translation>À propos de MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="643"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash and it codec in python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI d&apos;extraction, modification et de ré-encapsulage des pistes d&apos;un fichier matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Ce logiciel fait suite à plusieurs autres qui étaient codés en Bash et est codé en python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Ce logiciel est sous licence &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Un grand merci aux forums python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; pour leur patience.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), Novembre 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="649"/>
        <source>They talk about MKV Extractor Gui</source>
        <translation>Ils parlent de MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="650"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://sysads.co.uk/2014/09/install-mkv-extractor-qt-5-1-4-ubuntu-14-04/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;sysads.co.uk&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.softpedia.com/reviews/linux/mkv-extractor-qt-review-496919.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linux.softpedia.com/get/Multimedia/Video/MKV-Extractor-Qt-103555.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;linux.softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://zenway.ru/page/mkv-extractor-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;zenway.ru&lt;/span&gt;&lt;/a&gt; (Russian)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linuxg.net/how-to-install-mkv-extractor-qt-5-1-4-on-ubuntu-14-04-linux-mint-17-elementary-os-0-3-deepin-2014-and-other-ubuntu-14-04-derivatives/&quot;&gt;linuxg.net&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://la-vache-libre.org/mkv-extractor-gui-virer-les-sous-titres-inutiles-de-vos-fichiers-mkv-et-plus-encore/&quot;&gt;la-vache-libre.org&lt;/span&gt;&lt;/a&gt; (French)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://passionexubuntu.altervista.org/index.php/it/kubuntu/1152-mkv-extractor-qt-vs-5-1-3-kde.html&quot;&gt;passionexubuntu.altervista.org&lt;/span&gt;&lt;/a&gt; (Italian)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://sysads.co.uk/2014/09/install-mkv-extractor-qt-5-1-4-ubuntu-14-04/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;sysads.co.uk&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.softpedia.com/reviews/linux/mkv-extractor-qt-review-496919.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;softpedia.com&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linux.softpedia.com/get/Multimedia/Video/MKV-Extractor-Qt-103555.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;linux.softpedia.com&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://zenway.ru/page/mkv-extractor-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;zenway.ru&lt;/span&gt;&lt;/a&gt; (Russe)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linuxg.net/how-to-install-mkv-extractor-qt-5-1-4-on-ubuntu-14-04-linux-mint-17-elementary-os-0-3-deepin-2014-and-other-ubuntu-14-04-derivatives/&quot;&gt;linuxg.net&lt;/span&gt;&lt;/a&gt; (Anglais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://la-vache-libre.org/mkv-extractor-gui-virer-les-sous-titres-inutiles-de-vos-fichiers-mkv-et-plus-encore/&quot;&gt;la-vache-libre.org&lt;/span&gt;&lt;/a&gt; (Fraçais)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://passionexubuntu.altervista.org/index.php/it/kubuntu/1152-mkv-extractor-qt-vs-5-1-3-kde.html&quot;&gt;passionexubuntu.altervista.org&lt;/span&gt;&lt;/a&gt; (Italien)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="652"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="653"/>
        <source>The command(s) have finished</source>
        <translation>Commande(s) terminée(s)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="654"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; command have finished its work.</source>
        <translation>La commande &lt;b&gt;{}&lt;/b&gt; a terminé son travail.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="655"/>
        <source>All commands have finished their work.</source>
        <translation>Toutes les commande ont terminé leur travail.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="657"/>
        <source>Use the right click for view options.</source>
        <translation>Utiliser le clic droit pour voir les options.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="659"/>
        <source>All compatible Files</source>
        <translation>Tous les Fichiers compatibles</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="660"/>
        <source>Matroska Files</source>
        <translation>Fichiers Matroska</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="661"/>
        <source>Other Files</source>
        <translation>Autres Fichiers</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="663"/>
        <source>Do not ask again</source>
        <translation>Ne plus me demander</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="664"/>
        <source>File needs to be converted</source>
        <translation>Le fichier doit être converti</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="665"/>
        <source>This file is not supported by mkvmerge.
Do you want convert this file in mkv ?</source>
        <translation>Ce fichier n&apos;est pas supporté par mkvmerge.
Voulez-vous le convertir en mkv ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="666"/>
        <source>MKVMerge Warning</source>
        <translation>Alerte MKVMerge</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="667"/>
        <source>A warning has occurred during the convertion of the file, read the feedback informations.</source>
        <translation>Une alarme est survenue pendant la conversion du fichier, lisez les retours.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="668"/>
        <source>Do not warn me</source>
        <translation>Ne plus m&apos;avertir</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="669"/>
        <source>Choose the out folder of the new mkv file</source>
        <translation>Choix du dossier de sorti du nouveau fichier mkv</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="671"/>
        <source>Already existing file</source>
        <translation>Fichier déjà existant</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="672"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; is already existing, overwrite it?</source>
        <translation>Le fichier &lt;b&gt;{}&lt;/b&gt; existe déjà, faut-il l&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="674"/>
        <source>Awaiting resume</source>
        <translation>Reprise en attente</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="675"/>
        <source>The software is &lt;b&gt;pausing&lt;/b&gt;.&lt;br/&gt;Thanks to clic on the &apos;&lt;b&gt;Resume work&lt;/b&gt;&apos; button or &apos;&lt;b&gt;Cancel work&lt;/b&gt;&apos; for cancel all the work and remove the temporary files.</source>
        <translation>Le logiciel est en &lt;b&gt;pause&lt;/b&gt;.&lt;br/&gt;Merci de cliquer sur le bouton &apos;&lt;b&gt;Reprise du travail&lt;/b&gt;&apos; ou &apos;&lt;b&gt;Annulation du travail&lt;/b&gt;&apos; pour annuler tout le travail et supprimer les fichiers temporaires.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="676"/>
        <source>Resume work</source>
        <translation>Reprise du travail</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="677"/>
        <source>Cancel work</source>
        <translation>Annulation du travail</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="679"/>
        <source>The last file doesn&apos;t exist</source>
        <translation>Le dernier fichier n&apos;existe plus</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="680"/>
        <source>You have checked the option who reload the last file to the launch of MKV Extractor Qt, but this last file doesn&apos;t exist anymore.</source>
        <translation>Vous avez coché l&apos;option qui recharge le dernier fichier au lancement de MKV Extractor Qt, mais ce fichier n&apos;existe plus.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="681"/>
        <source>Wrong arguments</source>
        <translation>Mauvais arguments</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="682"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; file given as argument does not exist.</source>
        <translation>Le fichier &lt;b&gt;{}&lt;/b&gt; donné en argument n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="683"/>
        <source>&lt;b&gt;Too many arguments given:&lt;/b&gt;&lt;br/&gt; - {} </source>
        <translation>&lt;b&gt;Trop d&apos;arguments donnés :&lt;/b&gt;&lt;br/&gt; - {} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="684"/>
        <source>Wrong value</source>
        <translation>Valeur erronée</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="685"/>
        <source>Wrong value for the &lt;b&gt;{}&lt;/b&gt; option, MKV Extractor Qt will use the default value.</source>
        <translation>Valeur erronée pour l&apos;option &lt;b&gt;{}&lt;/b&gt;, MKV Extractor Qt va utiliser la valeur par défaut.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="686"/>
        <source>Wrong path for the &lt;b&gt;{}&lt;/b&gt; option, MKV Extractor Qt will use the default path.</source>
        <translation>Emplacement erroné pour l&apos;option &lt;b&gt;{}&lt;/b&gt;, MKV Extractor Qt va utiliser l&apos;emplacement par défaut.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="687"/>
        <source>No way to open this file</source>
        <translation>Pas moyen d&apos;ouvrir ce fichier</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="688"/>
        <source>The file to open contains quotes (&quot;) in its name. It&apos;s impossible to open a file with this carac. Please rename it.</source>
        <translation>Le fichier à ouvrir contient des guillemets (&quot;) dans son nom, il est impossible d&apos;ouvrir un fichier avec ce caractére. Merci de le renommer.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="689"/>
        <source>Space available</source>
        <translation>Espace disponible</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="690"/>
        <source>Not enough space available in the &lt;b&gt;{}&lt;/b&gt; folder.&lt;br/&gt;It is advisable to have at least twice the size of free space on the disk file.&lt;br&gt;Free disk space: &lt;b&gt;{}&lt;/b&gt;.&lt;br&gt;File size: &lt;b&gt;{}&lt;/b&gt;.</source>
        <translation>Erreur : Pas assez de place disponible dans le dossier &lt;b&gt;{}&lt;/b&gt;.&lt;br/&gt;Il est conseillé d&apos;avoir au moins deux fois la taille du fichier d&apos;espace libre sur le disque.&lt;br&gt;Espace disque libre : &lt;b&gt;{}&lt;/b&gt;.&lt;br&gt;Taille du fichier : &lt;b&gt;{}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="691"/>
        <source>Not enough space available in &lt;b&gt;{}&lt;/b&gt; folder.&lt;br/&gt;Free space in the disk: &lt;b&gt;{}&lt;/b&gt;&lt;br/&gt;File size: &lt;b&gt;{}&lt;/b&gt;.</source>
        <translation>Erreur : Pas assez de place disponible dans le dossier&lt;b&gt;{}&lt;/b&gt;.&lt;br/&gt;Espace disque libre : &lt;b&gt;{}&lt;/b&gt;&lt;br/&gt;Taille du fichier : &lt;b&gt;{}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="693"/>
        <source>Help me!</source>
        <translation>À l&apos;aide !</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="694"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=1508741&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vous êtes perdu ? Vous avez besoin d&apos;aide ?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normalement toutes les informations nécessaires sont présentes:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Lisez les informations de la barre d&apos;état lors du déplacement de la souris sur les widgets&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Malgré tout, si vous avez besoin de plus amples informations:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Mon adresse mail : &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Merci de l&apos;intérêt que vous portez à ce logiciel.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="696"/>
        <source>Skip the existing file test.</source>
        <translation>Saute la verification d&apos;existence du fichier.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="697"/>
        <source>Quality of the ac3 file converted.</source>
        <translation>Qualité du fichier ac3 converti.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="698"/>
        <source>Power of the ac3 file converted.</source>
        <translation>Puissance du fichier ac3 converti.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="699"/>
        <source>Skip the free space disk test.</source>
        <translation>Saute la verification d&apos;espace libre.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="700"/>
        <source>View more informations in feedback box.</source>
        <translation>Afficher plus d&apos;informations dans la box de retours.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="701"/>
        <source>Delete temporary files.</source>
        <translation>Supprimer les fichiers temporaires.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="702"/>
        <source>Remove the error message if the last file doesn&apos;t exist.</source>
        <translation>Supprimer ce message d&apos;erreur si le dernier fichier n&apos;existe plus.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="703"/>
        <source>Show or hide the information feedback box.</source>
        <translation>Afficher ou cacher la box de retour d&apos;informations.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="704"/>
        <source>Anchor or loose information feedback box.</source>
        <translation>Ancrer ou détacher la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="705"/>
        <source>The folder to use for extract temporaly the attachements file to view them.</source>
        <translation>Dossier à utiliser pour extraire temporairement les fichiers joints et les visualiser.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="730"/>
        <source>Use FFMpeg for the conversion.</source>
        <translation>Utiliser FFMpeg pour la conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="707"/>
        <source>Keep in memory the last file opened for open it at the next launch of MKV Extractor Qt.</source>
        <translation>Conserver en mémoire le dernier fichier ouvert pour le prochain lancement de MKV Extractor Qt.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="708"/>
        <source>Software to use for just encapsulate.</source>
        <translation>Logiciel à utiliser pour simplement encapsuler.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="709"/>
        <source>Skip the proposal to softaware to use.</source>
        <translation>Sauter la proposition du logiciel à utiliser.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="710"/>
        <source>Skip the confirmation of the conversion.</source>
        <translation>Sauter la confirmation de la conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="711"/>
        <source>Hide the information of the conversion warning.</source>
        <translation>Cacher le retour d&apos;alarmes de conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="712"/>
        <source>Folder of the MKV files.</source>
        <translation>Dossier des fichiers MKV.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="713"/>
        <source>Output folder for the new MKV files.</source>
        <translation>Dossier de sorti pour les nouveaux fichiers MKV.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="714"/>
        <source>Software language to use.</source>
        <translation>Langue à utiliser pour le logiciel.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="715"/>
        <source>Remove the Qt file who keeps the list of the recent files for the window selection.</source>
        <translation>Supprime le fichier Qt qui conserve la liste des fichiers récents de la fenêtre de selection.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="716"/>
        <source>Use the same input and output folder.</source>
        <translation>Utiliser le même dossier en entrée et sortie.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="717"/>
        <source>Automatically rename the output file name in MEG_FileName.</source>
        <translation>Renommer automatiquement le fichier de sortie en MEG_NomDuFichier.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="718"/>
        <source>Switch to stereo during conversion.</source>
        <translation>Passer en stéréo lors de la conversion.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="719"/>
        <source>Opening subtitles before encapsulation.</source>
        <translation>Ouvre les sous-titres avant la ré-encapsulation.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="720"/>
        <source>Display or hide the system tray icon.</source>
        <translation>Afficher ou cacher l&apos;icone de la zone de notification.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="721"/>
        <source>Number of CPU to use with Tesseract, by default: max value.</source>
        <translation>Nombre de CPU à utiliser avec Tesseract, par défaut : valeur max.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="722"/>
        <source>Keep in memory the aspect and the position of the window for the next opened.</source>
        <translation>Conserver en mémoire l&apos;aspect et la position de la fenêtre pour le prochain lancement.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="724"/>
        <source>Number of CPU to use</source>
        <translation>Nombre de CPU à utiliser</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="725"/>
        <source>Choose the number of CPU to use with Tesseract.</source>
        <translation>Choisir le nombre de CPU à utiliser avec Tesseract.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="726"/>
        <source>Convert in AC3</source>
        <translation>Convertion en AC3</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="727"/>
        <source>Convert audio tracks automatically to AC3.</source>
        <translation>Convertit automatiquement les pistes audio en AC3.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="728"/>
        <source>Delete temporary files</source>
        <translation>Supprimer les fichiers temporaires de la ré-encapsulation</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="729"/>
        <source>The temporary files are the extracted tracks.</source>
        <translation>Les fichiers temporaires sont les pistes extraites.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="731"/>
        <source>Increase the sound power</source>
        <translation>Amplifier la puissance audio</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="732"/>
        <source>No power change.</source>
        <translation>Pas de modification de la puissance.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="733"/>
        <source>Multiplying audio power by {}.</source>
        <translation>Multiplie la puissance audio par {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="734"/>
        <source>Power x {}</source>
        <translation>Puissance x {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="735"/>
        <source>List of available flow rates of conversion</source>
        <translation>Liste des débits de conversion disponibles</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="736"/>
        <source>Convert the audio quality in {} kbits/s.</source>
        <translation>Convertit la qualité audio en {} kbits/s.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="737"/>
        <source>{} kbits/s</source>
        <translation>{} kbits/s</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="738"/>
        <source>Switch to stereo during conversion</source>
        <translation>Passer en stéréo lors de la conversion</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="739"/>
        <source>The audio will not use the same number of channels, the audio will be stereo (2 channels).</source>
        <translation>L&apos;audio ne conservera pas son nombre de canal, l&apos;audio sera en stéréo (2 canaux).</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="740"/>
        <source>Opening subtitles before encapsulation</source>
        <translation>Ouvrir les sous-titres avant la ré-encapsulation</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="741"/>
        <source>Auto opening of subtitle srt files for correction. The software will be paused.</source>
        <translation>Ouvre automatiquement les fichiers sous-titres srt pour les corriger. Le logiciel sera mis en pause.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="742"/>
        <source>New value for &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; option: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation>Nouvelle valeur pour l&apos;option &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="743"/>
        <source>No change the quality</source>
        <translation>Ne pas modifier la qualité</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="744"/>
        <source>The quality of the audio tracks will not be changed.</source>
        <translation>La qualité de la piste audio ne sera pas modifiée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="745"/>
        <source>No change the power</source>
        <translation>Ne pas modifier la puissance</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="746"/>
        <source>The power of the audio tracks will not be changed.</source>
        <translation>La puissance de la piste audio ne sera pas modifiée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="748"/>
        <source>Selected file: {}.</source>
        <translation>Fichier sélectionné : {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="749"/>
        <source>Selected folder: {}.</source>
        <translation>Dossier sélectionné : {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="750"/>
        <source>Always use the same output folder as the input MKV file (automatically updated)</source>
        <translation>Toujours utiliser le même dossier de sortie que celui d&apos;entré du fichier MKV (mis à jour automatiquement)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="752"/>
        <source>Keep in memory the last file opened for open it at the next launch of MKV Extractor Qt (to use for tests)</source>
        <translation>Conserver en mémoire le dernier fichier ouvert pour le prochain lancement de MKV Extractor Qt (util pour tester)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="753"/>
        <source>Select the input MKV File</source>
        <translation>Sélectionner le fichier MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="754"/>
        <source>Select the output MKV file</source>
        <translation>Séléctionner le fichier MKV de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="755"/>
        <source>Select the output folder</source>
        <translation>Sélectionner le dossier de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="757"/>
        <source>MKV Merge Gui or MKV Extractor Qt ?</source>
        <translation>MKV Merge Gui ou MKV Extractor Qt ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="758"/>
        <source>You want extract and reencapsulate the tracks without use other options.

If you just need to make this, you should use MMG (MKV Merge gui) who is more adapted for this job.

What software do you want use ?
</source>
        <translation>Vous voulez extraire et réencapsuler des pistes sans autres options.

Si vous voulez juste faire ça, vous devriez utiliser MMG (MKV Merge Gui) qui est plus adapté à ce travail.

Quel logiciel voulez-vous utiliser ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="760"/>
        <source>Always use the default file rename (MEG_FileName)</source>
        <translation>Toujours utiliser le renommage par défaut (MEG_NomDuFichier)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="761"/>
        <source>Choose the output file name</source>
        <translation>Choisir le dossier de sortie</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="763"/>
        <source>audio</source>
        <translation>audio</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="764"/>
        <source>subtitles</source>
        <translation>sous-titres</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="765"/>
        <source>video</source>
        <translation>vidéo</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="767"/>
        <source>If the remuxed file has reading problems, change this value.</source>
        <translation>Si le fichier ré-encapsulé à des problèmes de lecture, changer cette valeur.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="768"/>
        <source>Change the language if it&apos;s not right. &apos;und&apos; means &apos;Undetermined&apos;.</source>
        <translation>Changer la langue si ce n&apos;est pas la bonne. &apos;und&apos; signifie &apos;Indéterminé&apos;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="769"/>
        <source>This track can be renamed and must contain an extension to avoid reading errors by doubleclicking.</source>
        <translation>Cette piste peut être renommée et doit contenir une extension pour eviter les erreurs de lecture via un double clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="770"/>
        <source>chapters</source>
        <translation>chapitres</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="771"/>
        <source>Work with track number {}.</source>
        <translation>Travailler avec la piste numéro {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="772"/>
        <source>Work with attachment number {}.</source>
        <translation>Travailler sur le fichier joint n° {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="773"/>
        <source>Work with {}.</source>
        <translation>Travailler sur les {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="774"/>
        <source>This track can be renamed by doubleclicking.</source>
        <translation>Cette piste peut être renommée via un double clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="775"/>
        <source>tags</source>
        <translation>tags</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="776"/>
        <source>This track is a {} type and cannot be previewed.</source>
        <translation>Cette piste est de type {} et ne peut être prévisualisée.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="777"/>
        <source>This attachment file is a {} type, it can be extracted (speedy) and viewed by clicking.</source>
        <translation>Ce fichier joint est de type {}, il peut être extrait (rapide) et visualisé via un clic.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="778"/>
        <source>Change the fps value if needed. Useful in case of audio lag. Normal : 23.976, 25.000 and 30.000.</source>
        <translation>Changer le nombre d&apos;images par seconde si besoin. Utile en cas de décalage avec l&apos;audio.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="780"/>
        <source> All commands were canceled </source>
        <translation> Toutes les commandes ont été annulées </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source>Command execution: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation>Éxécution de la commande : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="782"/>
        <source> The last command returned an error </source>
        <translation> La dernière commande a renvoyé une erreur </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="783"/>
        <source> {} execution is finished </source>
        <translation> L&apos;éxécution de {} est términée </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="784"/>
        <source> MKV File Tracks </source>
        <translation> Pistes du fichier MKV </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="785"/>
        <source> {} execution in progress </source>
        <translation> L&apos;éxécution de {} est en cours </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="645"/>
        <source>About Qtesseract5</source>
        <translation>À propos de Qtesseract5</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="649"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;b&gt;Qtesseract5&lt;/b&gt; is a software who converts the IDX/SUB file in SRT (text) file. For that works, it use &lt;i&gt;subp2pgm&lt;/i&gt; (export the images files from SUB file), &lt;i&gt;Tesseract&lt;/i&gt; (for read their files) and &lt;i&gt;subptools&lt;/i&gt; (to create a SRT file).&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; &lt;hizo@free.fr&gt;, April 2016&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;b&gt;Qtesseract5&lt;/b&gt; est un logiciel qui converti les fichiers IDX/SUB en fichier SRT (texte). Pour fonctionner, il utilise &lt;i&gt;subp2pgm&lt;/i&gt; (extrait les images depuis le fichier SUB), &lt;i&gt;Tesseract&lt;/i&gt; (pour lire les ces fichiers) et &lt;i&gt;subptools&lt;/i&gt; (pour créer un fichier SRT).&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; &lt;hizo@free.fr&gt;, Avril 2016&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="646"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;b&gt;Qtesseract5&lt;/b&gt; is a software who converts the IDX/SUB file in SRT (text) file. For that works, it use &lt;i&gt;subp2pgm&lt;/i&gt; (export the images files from SUB file), &lt;i&gt;Tesseract&lt;/i&gt; (for read their files) and &lt;i&gt;subptools&lt;/i&gt; (to create a SRT file).&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://forum.ubuntu-fr.org/viewtopic.php?pid=21507283&quot;&gt;Topic on ubuntu-fr.org&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; &lt;hizo@free.fr&gt;, April 2016&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;b&gt;Qtesseract5&lt;/b&gt; est un logiciel qui converti les fichiers IDX/SUB en fichier SRT (texte). Pour fonctionner, il utilise &lt;i&gt;subp2pgm&lt;/i&gt; (extrait les images depuis le fichier SUB), &lt;i&gt;Tesseract&lt;/i&gt; (pour lire les ces fichiers) et &lt;i&gt;subptools&lt;/i&gt; (pour créer un fichier SRT).&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://forum.ubuntu-fr.org/viewtopic.php?pid=21507283&quot;&gt;Topic sur ubuntu-fr.org&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; &lt;hizo@free.fr&gt;, Avril 2016&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QFileDIalogCustom</name>
    <message>
        <location filename="QFileDialogCustom.py" line="120"/>
        <source>Already existing file</source>
        <translation type="obsolete">Fichier déjà existant</translation>
    </message>
    <message>
        <location filename="QFileDialogCustom.py" line="120"/>
        <source>The &lt;b&gt;{}&lt;/b&lt; is already existing, overwrite it?</source>
        <translation type="obsolete">Le fichier &lt;b&gt;{}&lt;/b&gt; existe déjà, faut-il l&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="QFileDialogCustom.py" line="120"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; is already existing, overwrite it?</source>
        <translation type="obsolete">Le fichier &lt;b&gt;{}&lt;/b&gt; existe déjà, faut-il l&apos;écraser ?</translation>
    </message>
</context>
<context>
    <name>QTextEditCustom</name>
    <message>
        <location filename="MKVExtractorQt5.py" line="80"/>
        <source>Clean the information fee&amp;dback box</source>
        <translation>Nettoyer la box de retour &amp;d&apos;information</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="88"/>
        <source>&amp;Export info to ~/InfoMKVExtractorQt.txt</source>
        <translation type="obsolete">&amp;Exporter les infos dans le fichier ~/InfoMKVExtractorQt.txt</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="87"/>
        <source>&amp;Export info to ~/InfoMKVExtractorQt5.txt</source>
        <translation>&amp;Exporter les infos dans le fichier ~/InfoMKVExtractorQt5.txt</translation>
    </message>
</context>
<context>
    <name>mkv_extractor_qt</name>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="283"/>
        <source>Codec</source>
        <translation type="obsolete">Codec</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1083"/>
        <source>Launch extract/convert/mux tracks.</source>
        <translation type="obsolete">Lance l&apos;extraction/conversion/encapsulage des pistes.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="743"/>
        <source>Execute</source>
        <translation type="obsolete">Exécuter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="769"/>
        <source>Cancel running processes and delete resulting file.</source>
        <translation type="obsolete">Annule le processus en cours et supprime le fichier qui en résulte.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="772"/>
        <source>Stop</source>
        <translation type="obsolete">Arrêter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1062"/>
        <source>Quit the program and save the options.</source>
        <translation type="obsolete">Quitte le logiciel et sauvegarde les options.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1059"/>
        <source>Quit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="877"/>
        <source>File</source>
        <translation type="obsolete">Fichier</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="876"/>
        <source>Actions</source>
        <translation type="obsolete">Actions</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="910"/>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="916"/>
        <source>Options</source>
        <translation type="obsolete">Options</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1023"/>
        <source>Open an MKV file</source>
        <translation type="obsolete">Ouvrir un fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1041"/>
        <source>Output folder</source>
        <translation type="obsolete">Dossier de sortie</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1026"/>
        <source>Check MKV file</source>
        <translation type="obsolete">Vérifier le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1059"/>
        <source>About MKV Extractor Qt</source>
        <translation type="obsolete">À propos du logiciel</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1098"/>
        <source>About Qt</source>
        <translation type="obsolete">À propos de Qt</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1126"/>
        <source>Save MKV filename</source>
        <translation type="obsolete">Sauvegarder le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1134"/>
        <source>Display information about MKV Extractor Qt.</source>
        <translation type="obsolete">Ouvre une fenêtre d&apos;information sur le logiciel.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1173"/>
        <source>Display information about Qt.</source>
        <translation type="obsolete">Ouvre une fenêtre d&apos;information sur Qt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="305"/>
        <source>Remux</source>
        <translation type="obsolete">Ré-encapsuler</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="372"/>
        <source>DTS to AC3</source>
        <translation type="obsolete">De DTS vers AC3</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="164"/>
        <source>MKV Title</source>
        <translation type="obsolete">Titre du MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="155"/>
        <source>The title of the MKV file. This information can be edited. </source>
        <translation type="obsolete">Le titre du fichier MKV. Cette information peut être modifiée.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="331"/>
        <source>SUB to SRT</source>
        <translation type="obsolete">De SUB vers SRT</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="267"/>
        <source>Name/Information</source>
        <translation type="obsolete">Nom/Information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="275"/>
        <source>Language/fps/Size</source>
        <translation type="obsolete">Langue/fps/Taille</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1134"/>
        <source>Delete config file</source>
        <translation type="obsolete">Effacer le fichier de config</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="804"/>
        <source>Resume</source>
        <translation type="obsolete">Reprendre</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="400"/>
        <source>Subtitles recognized:</source>
        <translation type="obsolete">Reconnaissance des sous titres :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="486"/>
        <source>Previous subtitle image.</source>
        <translation type="obsolete">Image de sous-titre précédante.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="489"/>
        <source>Previous</source>
        <translation type="obsolete">Précédant</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="534"/>
        <source>Next subtitle image.</source>
        <translation type="obsolete">Image de sous-titre suivante.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="537"/>
        <source>Next</source>
        <translation type="obsolete">Suivant</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="843"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Creative Commons BY-NC-SA&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Ce logiciel est sous licence &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Creative Commons BY-NC-SA&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="875"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Créé par &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), Novembre 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1091"/>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="428"/>
        <source>Tesseract couldn&apos;t recognize the subtitle file. It must be done manually.</source>
        <translation type="obsolete">Tesseract n&apos;a pas pu reconnaître tous les sous-titres. Il faut finir manuellement.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="818"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/re-encapsulate the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI d&apos;extraction, modification et de ré-encapsulage des pistes d&apos;un fichier matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Ce logiciel fait suite à plusieurs autres qui étaient codés en Bash.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="856"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Un grand merci aux forums python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; pour leur patience.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1008"/>
        <source>Start job queue</source>
        <translation type="obsolete">Lancer le travail</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1101"/>
        <source>Verify Matroska files for specification conformance with MKValidator.</source>
        <translation type="obsolete">Vérifie la conformité des spécifications du fichier Matroska avec MKValidator.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1044"/>
        <source>Optimize MKV file</source>
        <translation type="obsolete">Optimiser le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1119"/>
        <source>Clean/Optimize Matroska files with MKClean.</source>
        <translation type="obsolete">Nettoie/Optimise le fichier Matroska qui est déjà encapsulé avec MKClean.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1149"/>
        <source>Edit MKV file</source>
        <translation type="obsolete">Éditer le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1224"/>
        <source>Open the MKV file with mkvmerge GUI for more modifications.</source>
        <translation type="obsolete">Ouvre le fichier MKV avec MKV Merge Gui pour plus de modifications.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1245"/>
        <source>Open the MKV file with the default application.</source>
        <translation type="obsolete">Ouvre le fichier MKV avec le logiciel par defaut.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1042"/>
        <source>Information feedback:</source>
        <translation type="obsolete">Retour d&apos;informations :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="130"/>
        <source>Available tracks in MKV file:</source>
        <translation type="obsolete">Pistes disponibles dans le fichier MKV :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="349"/>
        <source>Automatic conversion from vobsub to SRT.</source>
        <translation type="obsolete">Convertit automatiquement les sous-titres vobsub en srt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="897"/>
        <source>Open the license file with the default application.</source>
        <translation type="obsolete">Ouvre le fichier licence avec le logiciel par défaut.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="900"/>
        <source>License</source>
        <translation type="obsolete">Licence</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="819"/>
        <source>Let&apos;s continue the work once button is clicked.</source>
        <translation type="obsolete">Relance le travail, une fois bouton cliqué.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1080"/>
        <source>View MKV information</source>
        <translation type="obsolete">Voir les informations du MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1155"/>
        <source>Display information about the MKV file with mkvinfo.</source>
        <translation type="obsolete">Affiche les informations du MKV avec MKVInfo.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1129"/>
        <source>Keep the MKV file in memory for the next session.</source>
        <translation type="obsolete">Garde en mémoire le fichier MKV pour le prochain lancement du logiciel.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1119"/>
        <source>Use the same output folder as the MKV file</source>
        <translation type="obsolete">Utiliser le même dossier de sortie que le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1194"/>
        <source>Change the output folder automatically to use the same folder as the MKV file.</source>
        <translation type="obsolete">Adapte automatiquement le dossier de sortie à celui du fichier MKV.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1209"/>
        <source>Delete config file and don&apos;t save the new values for this time.</source>
        <translation type="obsolete">Efface le fichier de config et ne sauvegarde pas les nouvelles valeurs pour cette fois.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1170"/>
        <source>Replay MKV file</source>
        <translation type="obsolete">Lire le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="302"/>
        <source>Automatically remux extracted tracks into a new MKV file. Options are available.</source>
        <translation type="obsolete">Ré-encapsule automatiquement les pistes extraitent dans un nouveau fichier mkv. Options disponibles.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="369"/>
        <source>Convert DTS tracks automatically to AC3, which is much more compatible. Options are available.</source>
        <translation type="obsolete">Conversion automatique des pistes DTS en AC3, format lisible par de nombreux instruments. Options disponibles.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="928"/>
        <source>Close about tab.</source>
        <translation type="obsolete">Fermer l&apos;onglet à propos.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1085"/>
        <source>Close help tab.</source>
        <translation type="obsolete">Fermer l&apos;onglet d&apos;aide.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1185"/>
        <source>Need help</source>
        <translation type="obsolete">Besoin d&apos;aide</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1197"/>
        <source>Clean the information feedback box</source>
        <translation type="obsolete">Nettoyer la box de retour d&apos;information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1026"/>
        <source>Chosing an MKV file is required.</source>
        <translation type="obsolete">La séléction d&apos;un fichier mkv est obligatoire.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1275"/>
        <source>Reset the information feedback box.</source>
        <translation type="obsolete">Réinitialisation de la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1044"/>
        <source>Chosing an output folder is mandatory if the option to use the same folder as the MKV file is not used.</source>
        <translation type="obsolete">La séléction d&apos;un dossier de sortie est obligatoire si l&apos;option utilisant le même dossier de sortie que celui du fichier mkv n&apos;est pas utilisée.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1041"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vous êtes perdu ? Vous avez besoin d&apos;aide ?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normalement toutes les informations nécessaires sont présentes:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Lisez les informations de la barre d&apos;état lors du déplacement de la souris sur les widgets&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Malgré tout, si vous avez besoin de plus amples informations:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Mon adresse mail : &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Merci de l&apos;intérêt que vous portez à ce logiciel.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1113"/>
        <source>Export info to ~/InfoMKVExtractorQt.txt.</source>
        <translation type="obsolete">Exporte les infos dans le fichier ~/InfoMKVExtractorQt.txt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1424"/>
        <source>Use avconv</source>
        <translation type="obsolete">Utiliser avconv</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1432"/>
        <source>Use ffmpeg</source>
        <translation type="obsolete">utiliser ffmpeg</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="990"/>
        <source>Hide / Show the information feedback box.</source>
        <translation type="obsolete">Cache / Affiche la box de retour d&apos;informations.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="801"/>
        <source>Press button to resume.</source>
        <translation type="obsolete">Appuyer sur le bouton pour reprendre.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="871"/>
        <source>F&amp;ile</source>
        <translation type="obsolete">F&amp;ichier</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="899"/>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Aide</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1017"/>
        <source>&amp;Open an MKV file</source>
        <translation type="obsolete">&amp;Ouvrir un fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1038"/>
        <source>Output &amp;folder</source>
        <translation type="obsolete">&amp;Dossier de sortie</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1056"/>
        <source>&amp;Quit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1080"/>
        <source>&amp;Start job queue</source>
        <translation type="obsolete">&amp;Lancer le travail</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1098"/>
        <source>&amp;Check MKV file</source>
        <translation type="obsolete">&amp;Vérifier le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1116"/>
        <source>&amp;Optimize MKV file</source>
        <translation type="obsolete">&amp;Optimiser le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1131"/>
        <source>&amp;About MKV Extractor Qt</source>
        <translation type="obsolete">&amp;À propos du logiciel</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1152"/>
        <source>&amp;View MKV information</source>
        <translation type="obsolete">&amp;Voir les informations du MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1170"/>
        <source>About &amp;Qt</source>
        <translation type="obsolete">À propos de &amp;Qt</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1191"/>
        <source>&amp;Use the same output folder as the MKV file</source>
        <translation type="obsolete">&amp;Utiliser le même dossier de sortie que le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="817"/>
        <source>&amp;Delete config file</source>
        <translation type="obsolete">&amp;Supprimer le fichier de config</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1221"/>
        <source>&amp;Edit MKV file</source>
        <translation type="obsolete">&amp;Editer le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1242"/>
        <source>&amp;Replay MKV file</source>
        <translation type="obsolete">&amp;Lire le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1257"/>
        <source>&amp;Need help</source>
        <translation type="obsolete">&amp;Besoin d&apos;aide</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1272"/>
        <source>&amp;Clean the information feedback box</source>
        <translation type="obsolete">&amp;Nettoyer la box de retour d&apos;information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1225"/>
        <source>Delete recent files/URLs file (qt file)</source>
        <translation type="obsolete">Supprimer le fichier des fichiers/URLs &amp;récents (fichier qt)</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="573"/>
        <source>Configuration  edition</source>
        <translation type="obsolete">Édition de la configuration</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="626"/>
        <source>Values</source>
        <translation type="obsolete">Valeurs</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="631"/>
        <source>Default Values</source>
        <translation type="obsolete">Valeurs par défaut</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1471"/>
        <source>Delete &amp;recent files/URLs file (qt file)</source>
        <translation type="obsolete">Supprimer le fichier qt des fichiers/URLS &amp;récents</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="674"/>
        <source>Delete the actual value will use the default value</source>
        <translation type="obsolete">Effacer la valeur actuelle utilisera la valeur par defaut</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="650"/>
        <source>&amp;Reset</source>
        <translation type="obsolete">&amp;Réinitialiser</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1206"/>
        <source>C&amp;onfiguration  edition</source>
        <translation type="obsolete">Édition de la c&amp;onfiguration</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1077"/>
        <source>Clean the information feedback box.</source>
        <translation type="obsolete">Nettoyer la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1290"/>
        <source>Delete &amp;recent files/URLs list</source>
        <translation type="obsolete">Supprimer la liste des fichiers/URLS &amp;récents</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1474"/>
        <source>Remove the Qt file who keeps the list of the recent files for the file selection.</source>
        <translation type="obsolete">Supprime le fichier Qt qui conserve la liste des fichiers récents de la fenêtre de selection.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1293"/>
        <source>Remove the Qt file who keeps the list of the recent files for the window selection.</source>
        <translation type="obsolete">Supprime le fichier Qt qui conserve la liste des fichiers récents de la fenêtre de selection.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="866"/>
        <source>Fi&amp;le</source>
        <translation type="obsolete">F&amp;ichier</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="971"/>
        <source>&amp;Information feedback:</source>
        <translation type="obsolete">Retour d&apos;&amp;informations :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1308"/>
        <source>View &amp;information feedback box</source>
        <translation type="obsolete">Afficher la box de retour d&apos;&amp;informations</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1373"/>
        <source>Keep the window aspect and its position</source>
        <translation type="obsolete">Conserver l&apos;aspect et la position de la fenêtre</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1344"/>
        <source>Keep in memory the aspect and the position of the window for the next opened.</source>
        <translation type="obsolete">Conserver en mémoire l&apos;aspect et la position de la fenêtre pour le prochain lancement.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1311"/>
        <source>Show or hide the information feedback box.</source>
        <translation type="obsolete">Afficher ou cacher la box de retour d&apos;informations.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1325"/>
        <source>&amp;Fix information feedback box</source>
        <translation type="obsolete">&amp;Fixer la box de retour d&apos;informations</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1359"/>
        <source>Fix the information feedback information box.</source>
        <translation type="obsolete">Fixe la box de retour d&apos;informations</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1341"/>
        <source>&amp;Keep the window aspect and its position</source>
        <translation type="obsolete">Conserver l&apos;aspect et la position de la &amp;fenêtre</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1323"/>
        <source>&amp;Anchor information feedback box</source>
        <translation type="obsolete">&amp;Ancrer la box de retour d&apos;information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1326"/>
        <source>Anchor or loose information feedback box.</source>
        <translation type="obsolete">Ancrer ou détacher la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1356"/>
        <source>&amp;Debug mode</source>
        <translation type="obsolete">Mode &amp;Debug</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1359"/>
        <source>Enable this option for view more informations in feedback box.</source>
        <translation type="obsolete">Activer cette option pour afficher plus d&apos;informations dans la box de retour.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="357"/>
        <source>Audio Convert</source>
        <translation type="obsolete">Convertion Audio</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="789"/>
        <source>Pause</source>
        <translation type="obsolete">Pause</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="786"/>
        <source>Press button for pause between 2 jobs.</source>
        <translation type="obsolete">Cliquer pour mettre en pause entre 2 actions.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="354"/>
        <source>Convertion audio option.</source>
        <translation type="obsolete">Options de la convertion audio.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1379"/>
        <source>Use a system tray icon</source>
        <translation type="obsolete">Utiliser une icone dans la zone de notification</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1377"/>
        <source>Display a system tray icon of the software.</source>
        <translation type="obsolete">Afficher une icone du logiciel dans la zone de notification.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="328"/>
        <source>Convertion subtitles option.</source>
        <translation type="obsolete">Option de conversion des sous titres.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1374"/>
        <source>&amp;Use a system tray icon</source>
        <translation type="obsolete">&amp;Utiliser une icone de notification</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1385"/>
        <source>About MKVToolNix</source>
        <translation type="obsolete">À propos de MKVToolNix</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1391"/>
        <source>&amp;MKVToolNix last version</source>
        <translation type="obsolete">Dernière version de &amp;MKVToolNix</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1399"/>
        <source>They talk about</source>
        <translation type="obsolete">Ils en parlent</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1260"/>
        <source>Help me! I&apos;m lost :(</source>
        <translation type="obsolete">Aidez moi ! Je suis perdu :(</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1394"/>
        <source>Open the download page of MKVToolNix.</source>
        <translation type="obsolete">Ouvrir la page de téléchargement de MKVToolNix.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1406"/>
        <source>&amp;They talk about</source>
        <translation type="obsolete">Ils en parlen&amp;t</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1409"/>
        <source>They talk about MKV Extractor Gui.</source>
        <translation type="obsolete">Ils parlent de MKV Extractor Gui.</translation>
    </message>
</context>
<context>
    <name>mkv_extractor_qt5</name>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="130"/>
        <source>Available tracks in MKV file:</source>
        <translation>Pistes disponibles dans le fichier MKV :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="155"/>
        <source>The title of the MKV file. This information can be edited. </source>
        <translation>Le titre du fichier MKV. Cette information peut être modifiée.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="164"/>
        <source>MKV Title</source>
        <translation>Titre du MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="267"/>
        <source>Name/Information</source>
        <translation>Nom/Information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="275"/>
        <source>Language/fps/Size</source>
        <translation>Langue/fps/Taille</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="283"/>
        <source>Codec</source>
        <translation>Codec</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="302"/>
        <source>Automatically remux extracted tracks into a new MKV file. Options are available.</source>
        <translation>Ré-encapsule automatiquement les pistes extraitent dans un nouveau fichier mkv. Options disponibles.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="305"/>
        <source>Remux</source>
        <translation>Ré-encapsuler</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="328"/>
        <source>Convertion subtitles option.</source>
        <translation>Option de conversion des sous titres.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="331"/>
        <source>SUB to SRT</source>
        <translation>De SUB vers SRT</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="354"/>
        <source>Convertion audio option.</source>
        <translation>Options de la convertion audio.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="357"/>
        <source>Audio Convert</source>
        <translation>Convertion Audio</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="400"/>
        <source>Subtitles recognized:</source>
        <translation>Reconnaissance des sous titres :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="428"/>
        <source>Tesseract couldn&apos;t recognize the subtitle file. It must be done manually.</source>
        <translation>Tesseract n&apos;a pas pu reconnaître tous les sous-titres. Il faut finir manuellement.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="486"/>
        <source>Previous subtitle image.</source>
        <translation>Image de sous-titre précédante.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="489"/>
        <source>Previous</source>
        <translation>Précédant</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="534"/>
        <source>Next subtitle image.</source>
        <translation>Image de sous-titre suivante.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="537"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="573"/>
        <source>Configuration  edition</source>
        <translation>Édition de la configuration</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="917"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="626"/>
        <source>Values</source>
        <translation>Valeurs</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="631"/>
        <source>Default Values</source>
        <translation>Valeurs par défaut</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="650"/>
        <source>&amp;Reset</source>
        <translation>&amp;Réinitialiser</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="674"/>
        <source>Delete the actual value will use the default value</source>
        <translation>Effacer la valeur actuelle utilisera la valeur par defaut</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="700"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1084"/>
        <source>Launch extract/convert/mux tracks.</source>
        <translation>Lance l&apos;extraction/conversion/encapsulage des pistes.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="743"/>
        <source>Execute</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="769"/>
        <source>Cancel running processes and delete resulting file.</source>
        <translation>Annule le processus en cours et supprime le fichier qui en résulte.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="772"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="786"/>
        <source>Press button for pause between 2 jobs.</source>
        <translation type="obsolete">Cliquer pour mettre en pause entre 2 actions.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="789"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1063"/>
        <source>Quit the program and save the options.</source>
        <translation>Quitte le logiciel et sauvegarde les options.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1060"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="866"/>
        <source>Fi&amp;le</source>
        <translation>F&amp;ichier</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="876"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="899"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="972"/>
        <source>&amp;Information feedback:</source>
        <translation>Retour d&apos;&amp;informations :</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1018"/>
        <source>&amp;Open an MKV file</source>
        <translation>&amp;Ouvrir un fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1024"/>
        <source>Open an MKV file</source>
        <translation>Ouvrir un fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1027"/>
        <source>Chosing an MKV file is required.</source>
        <translation>La séléction d&apos;un fichier mkv est obligatoire.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1039"/>
        <source>Output &amp;folder</source>
        <translation>&amp;Dossier de sortie</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1042"/>
        <source>Output folder</source>
        <translation>Dossier de sortie</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1045"/>
        <source>Chosing an output folder is mandatory if the option to use the same folder as the MKV file is not used.</source>
        <translation>La séléction d&apos;un dossier de sortie est obligatoire si l&apos;option utilisant le même dossier de sortie que celui du fichier mkv n&apos;est pas utilisée.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1057"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1081"/>
        <source>&amp;Start job queue</source>
        <translation>&amp;Lancer le travail</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1099"/>
        <source>&amp;Check MKV file</source>
        <translation>&amp;Vérifier le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1102"/>
        <source>Verify Matroska files for specification conformance with MKValidator.</source>
        <translation>Vérifie la conformité des spécifications du fichier Matroska avec MKValidator.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1117"/>
        <source>&amp;Optimize MKV file</source>
        <translation>&amp;Optimiser le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1120"/>
        <source>Clean/Optimize Matroska files with MKClean.</source>
        <translation>Nettoie/Optimise le fichier Matroska qui est déjà encapsulé avec MKClean.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1132"/>
        <source>&amp;About MKV Extractor Qt</source>
        <translation>&amp;À propos du logiciel</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1135"/>
        <source>Display information about MKV Extractor Qt.</source>
        <translation type="obsolete">Ouvrir une fenêtre d&apos;information sur le logiciel.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1153"/>
        <source>&amp;View MKV information</source>
        <translation>&amp;Voir les informations du MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1156"/>
        <source>Display information about the MKV file with mkvinfo.</source>
        <translation>Affiche les informations du MKV avec MKVInfo.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1171"/>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1174"/>
        <source>Display information about Qt.</source>
        <translation>Ouvre une fenêtre d&apos;information sur Qt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1192"/>
        <source>&amp;Use the same output folder as the MKV file</source>
        <translation>&amp;Utiliser le même dossier de sortie que le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1195"/>
        <source>Change the output folder automatically to use the same folder as the MKV file.</source>
        <translation>Adapte automatiquement le dossier de sortie à celui du fichier MKV.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1207"/>
        <source>C&amp;onfiguration  edition</source>
        <translation>Édition de la c&amp;onfiguration</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1210"/>
        <source>Delete config file and don&apos;t save the new values for this time.</source>
        <translation>Efface le fichier de config et ne sauvegarde pas les nouvelles valeurs pour cette fois.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1222"/>
        <source>&amp;Edit MKV file</source>
        <translation>&amp;Editer le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1225"/>
        <source>Open the MKV file with mkvmerge GUI for more modifications.</source>
        <translation>Ouvre le fichier MKV avec MKV Merge Gui pour plus de modifications.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1243"/>
        <source>&amp;Replay MKV file</source>
        <translation>&amp;Lire le fichier MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1246"/>
        <source>Open the MKV file with the default application.</source>
        <translation>Ouvre le fichier MKV avec le logiciel par defaut.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1258"/>
        <source>&amp;Need help</source>
        <translation>&amp;Besoin d&apos;aide</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1261"/>
        <source>Help me! I&apos;m lost :(</source>
        <translation>Aide moi ! Je suis perdu :(</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1273"/>
        <source>&amp;Clean the information feedback box</source>
        <translation>&amp;Nettoyer la box de retour d&apos;information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1276"/>
        <source>Reset the information feedback box.</source>
        <translation>Réinitialisation de la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1291"/>
        <source>Delete &amp;recent files/URLs list</source>
        <translation>Supprimer la liste des fichiers/URLS &amp;récents</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1294"/>
        <source>Remove the Qt file who keeps the list of the recent files for the window selection.</source>
        <translation>Supprime le fichier Qt qui conserve la liste des fichiers récents de la fenêtre de selection.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1309"/>
        <source>View &amp;information feedback box</source>
        <translation>Afficher la box de retour d&apos;&amp;informations</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1312"/>
        <source>Show or hide the information feedback box.</source>
        <translation>Afficher ou cacher la box de retour d&apos;informations.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1324"/>
        <source>&amp;Anchor information feedback box</source>
        <translation>&amp;Ancrer la box de retour d&apos;information</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1327"/>
        <source>Anchor or loose information feedback box.</source>
        <translation>Ancrer ou détacher la box de retour d&apos;information.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1342"/>
        <source>&amp;Keep the window aspect and its position</source>
        <translation>Conserver l&apos;aspect et la position de la &amp;fenêtre</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1345"/>
        <source>Keep in memory the aspect and the position of the window for the next opened.</source>
        <translation>Conserver en mémoire l&apos;aspect et la position de la fenêtre pour le prochain lancement.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1357"/>
        <source>&amp;Debug mode</source>
        <translation>Mode &amp;Debug</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1360"/>
        <source>Enable this option for view more informations in feedback box.</source>
        <translation>Activer cette option pour afficher plus d&apos;informations dans la box de retour.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1375"/>
        <source>&amp;Use a system tray icon</source>
        <translation>&amp;Utiliser une icone de notification</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1378"/>
        <source>Display a system tray icon of the software.</source>
        <translation>Afficher une icone du logiciel dans la zone de notification.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1383"/>
        <source>Ass&amp;istant</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1392"/>
        <source>&amp;MKVToolNix last version</source>
        <translation>Dernière version de &amp;MKVToolNix</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1395"/>
        <source>Open the download page of MKVToolNix.</source>
        <translation>Ouvre la page de téléchargement de MKVToolNix.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1407"/>
        <source>&amp;They talk about</source>
        <translation>Ils en parlen&amp;t</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1410"/>
        <source>They talk about MKV Extractor Gui.</source>
        <translation>Ils parlent de MKV Extractor Gui.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1437"/>
        <source>Use avconv</source>
        <translation>Utiliser avconv</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1445"/>
        <source>Use ffmpeg</source>
        <translation>utiliser ffmpeg</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1422"/>
        <source>A&amp;bout Qtesseract5</source>
        <translation>À pr&amp;opos de Qtesseract5</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1135"/>
        <source>Display information about MKV Extractor Qt5.</source>
        <translation>Ouvre une fenêtre d&apos;information sur MKV Extractor Qt5.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1425"/>
        <source>Display information about Qtesseract5.</source>
        <translation>Ouvre une fenêtre d&apos;information sur Qtesseract5.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="786"/>
        <source>Press button for pause between 2 jobs or during the SUB to SRT convert.</source>
        <translation>Cliquer pour mettre en pause entre 2 actions ou pendant la conversion de SUB vers SRT.</translation>
    </message>
</context>
</TS>
