<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="cs_CZ" sourcelanguage="">
<context>
    <name>MKVExtractorQt</name>
    <message>
        <location filename="MKVExtractorQt5.py" line="773"/>
        <source>tags</source>
        <translation type="obsolete">Značky</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="788"/>
        <source>Advanced Audio Coding is a standardized, lossy compression and encoding scheme for digital audio.</source>
        <translation type="obsolete">Advanced Audio Coding je standard ztrátové komprese a systéme kódování digitálního zvuku.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="687"/>
        <source>Space available</source>
        <translation type="obsolete">Dostupné místo</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="766"/>
        <source>Change the language if it&apos;s not right. &apos;und&apos; means &apos;Undetermined&apos;.</source>
        <translation type="obsolete">měnit jazyk, pokud není správný. &apos;und&apos; znamená &apos;nestanovený&apos;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="726"/>
        <source>Delete temporary files</source>
        <translation type="obsolete">Smazat dočasné soubory opětovného zabalení</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="869"/>
        <source>This track is a {} type.</source>
        <translation type="obsolete">Tato stopa je typu {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="789"/>
        <source>The cook codec is a lossy audio compression codec developed by RealNetworks.</source>
        <translation type="obsolete">Kodek cook je formát ztrátové komprese zvuku vyvinutý RealNetworks.</translation>
    </message>
    <message>
        <location filename="" line="22121431"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v5.1.1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded Bash.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;Creative Commons BY-NC-SA&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;A big thank to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Create by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v5.1.1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Rozhraní pro vytahování, úpravy a opětovné zabalení stop souboru matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Tento program následuje po několika dalších, jež byly programovány v bashi.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tento program je licencován pod &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;Creative Commons BY-NC-SA&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Velké poděkování fórům pro python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; za jejich trpělivost.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Vytvořeno &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), listopad 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="644"/>
        <source>About MKV Extractor Gui</source>
        <translation type="obsolete">O rozhraní MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="735"/>
        <source>{} kbits/s</source>
        <translation type="obsolete">{} kbits/s</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="778"/>
        <source> All commands were canceled </source>
        <translation type="obsolete"> Všechny příkazy byly zrušeny </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="841"/>
        <source>Selected folder: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Vybraná složka: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="763"/>
        <source>SubStation Alpha is a subtitle file format created by CS Low that allows for more advanced subtitles than SRT format.</source>
        <translation type="obsolete">SubStation Alpha je formát titulků vytvořený CS Low, který umožňuje pokročilejší efekty než formát SRT.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="780"/>
        <source> The last command returned an error </source>
        <translation type="obsolete"> Poslední příkaz vrátil chybu </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="731"/>
        <source>Multiplying audio power by {}.</source>
        <translation type="obsolete">Znásobení zvukového výkonu {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="737"/>
        <source>The audio will not use the same number of channels, the audio will be stereo (2 channels).</source>
        <translation type="obsolete">Zvuk nezachová stejný počet kanálů, zvuk bude stereo (2 kanály).</translation>
    </message>
    <message>
        <location filename="" line="22120031"/>
        <source>Choose the good number of frames per second if needed. Useful in case of audio lag. Normal : 23.976, 25.000 and 30.000.</source>
        <translation type="obsolete">V případě potřeby vybrat dobrý počet obrázků za sekundu. Užitečné v případě prodlevy zvuku. Normální: 23.976, 25.000 a 30.000.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="739"/>
        <source>Auto opening of subtitle srt files for correction. The software will be paused.</source>
        <translation type="obsolete">Otevřít automaticky soubory titulků srt pro jejich opravení. Tento program bude pozastaven.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="779"/>
        <source>Command execution: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Provedení příkazu: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="753"/>
        <source>Select the output folder</source>
        <translation type="obsolete">Vybrat výstupní složku</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="736"/>
        <source>Switch to stereo during conversion</source>
        <translation type="obsolete">Během převodu přepnout do sterea</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="754"/>
        <source>Subtitle Bitmap.</source>
        <translation type="obsolete">Bitmapa titulků.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1076"/>
        <source>No information.</source>
        <translation type="obsolete">Žádné informace.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="867"/>
        <source>This track can be renamed.</source>
        <translation type="obsolete">Tato stopa může být přejmenována.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="787"/>
        <source>RealVideo 4.0, suspected to be based on H.264. RV40 is RealNetworks&apos; proprietary codec.</source>
        <translation type="obsolete">Předpokládá se, že RealVideo 4.0 je založeno na kodeku H264. RV40 je kodek vlastněný RealNetwork&apos;. </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="910"/>
        <source>SRT subtitle creation.</source>
        <translation type="obsolete">vytvoření souboru titulků SRT.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="771"/>
        <source>Vob Subtitle.</source>
        <translation type="obsolete">Titulky vob.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="740"/>
        <source>New value for &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; option: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">Nová hodnota pro &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="747"/>
        <source>Selected folder: {}.</source>
        <translation type="obsolete">Vybraná složka: {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="728"/>
        <source>Use FFMpeg for the conversion.</source>
        <translation type="obsolete">Použít FFMpeg pro převod.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="862"/>
        <source>This track can be displayed via an icon click.</source>
        <translation type="obsolete">Tato stopa může být zobrazena pomocí klepnutí na ikonu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source>MP3 is an encoding format for digital audio which uses a form of lossy data compression.</source>
        <translation type="obsolete">Formát MP3 je formát kódování digitálního zvuku, který používá formu ztrátové komprese dat.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="734"/>
        <source>Convert the audio quality in {} kbits/s.</source>
        <translation type="obsolete">Převede kvalitu zvuku na {} kbits/s.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="916"/>
        <source> Paused software time to read the subtitles </source>
        <translation type="obsolete"> Pozastavený čas programu ke čtení titulků </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1055"/>
        <source>Subtitle converter to images.</source>
        <translation type="obsolete">Převod titulků na obrázky.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="839"/>
        <source>Selected file: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation type="obsolete">vybraný soubor: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="733"/>
        <source>List of available flow rates of conversion</source>
        <translation type="obsolete">Seznam dostupných převodních datových toků</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="738"/>
        <source>Opening subtitles before encapsulation</source>
        <translation type="obsolete">Otevřít titulky před zabalením</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="863"/>
        <source>This track can not be displayed.</source>
        <translation type="obsolete">Tuto stopa nelze zobrazit.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="870"/>
        <source>Tesseract langs error</source>
        <translation type="obsolete">Chyba jazyku s Tesseractem</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="877"/>
        <source>Image progression : {} on {}</source>
        <translation type="obsolete">Zpracování obrázků: {} na {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="742"/>
        <source>DTS is a series of multichannel audio technologies owned by DTS, Inc.</source>
        <translation type="obsolete">DTS je řada vícekanálových technologií vyvinutá a vlastněná DTS, Inc.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="876"/>
        <source>Edition of files who have the md5: {}</source>
        <translation type="obsolete">Úprava souborů majících md5 : {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="679"/>
        <source>Wrong arguments</source>
        <translation type="obsolete">Špatné argumenty</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="770"/>
        <source>Work with attachment number {}.</source>
        <translation type="obsolete">Pracovat na připojeném souboru číslo {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="765"/>
        <source>If the remuxed file has reading problems, change this value.</source>
        <translation type="obsolete">Pokud má nově zabalený soubor potíže se čtením, změňte tuto hodnotu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="783"/>
        <source> {} execution in progress </source>
        <translation type="obsolete"> Probíhá provedení {} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="751"/>
        <source>Select the input MKV File</source>
        <translation type="obsolete">Vybrat vstupní soubor MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="811"/>
        <source>&lt;b&gt;Too many arguments given:&lt;/b&gt; {}</source>
        <translation type="obsolete">&lt;b&gt;Zadáno příliš mnoho argumentů:&lt;/b&gt; {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source>Not enough space available.

It is advisable to have at least twice the size of free space on the disk file.</source>
        <translation type="obsolete">Chyba: Nedostatek dostupného místa.

Doporučuje se mít na disku volného místa alespoň dvojnásobek velikosti souboru.</translation>
    </message>
    <message>
        <location filename="" line="22120115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vous êtes perdu ? Vous avez besoin d&apos;aide ?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Obvykle jsou přítomny všechny nutné informace:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Čtěte informace ve stavovém řádku, když přejíždíte ukazovátkem myši nad prvky rozhraní&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Pokud máte přesto potřebu ještě dalších informací:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Moje e-mailová adresa: &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Děkuji vám za zájem, který věnujete tomuto programu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source> {} execution is finished </source>
        <translation type="obsolete"> Provedení {} je dokončeno </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="691"/>
        <source>Help me!</source>
        <translation type="obsolete">Pomozte!</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="732"/>
        <source>Power x {}</source>
        <translation type="obsolete">Výkon x {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="750"/>
        <source>Dolby Digital audio codec, the audio compression is lossy.</source>
        <translation type="obsolete">Kodek audio Dolby Digital, ztrátová komprese zvuku.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="729"/>
        <source>Increase the sound power</source>
        <translation type="obsolete">Zvětšit zvukový výkon</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="752"/>
        <source>Select the output MKV file</source>
        <translation type="obsolete">Vybrat výstupní soubor MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="769"/>
        <source>Work with track number {}.</source>
        <translation type="obsolete">Pracovat se stopou číslo {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="746"/>
        <source>Selected file: {}.</source>
        <translation type="obsolete">Vybraný soubor: {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="871"/>
        <source>The subtitle language is not avaible in Tesseract list langs: {}</source>
        <translation type="obsolete">jazyk titulků není dostupný v seznamu jazyků Tesseractu:{} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="736"/>
        <source>Ogg is a free, open container format maintained by the Xiph.Org Foundation.</source>
        <translation type="obsolete">Ogg je svobodný, otevřený kontejner udržovaný nadací Xiph.Org.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="911"/>
        <source>All subtitles images could not be recognized.
We must therefore specify the missing texts.</source>
        <translation type="obsolete">Všechny obrázkové titulky se nepodařilo rozpoznat.
Je tudíž potřeba přikročit ke stanovení chybějících textů.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="768"/>
        <source>chapters</source>
        <translation type="obsolete">Kapitoly</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="730"/>
        <source>No power change.</source>
        <translation type="obsolete">Žádná změna výkonu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="771"/>
        <source>Work with {}.</source>
        <translation type="obsolete">Pracovat na {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="727"/>
        <source>The temporary files are the extracted tracks.</source>
        <translation type="obsolete">Dočasné soubory jsou vytažené stopy.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="745"/>
        <source>H264 is a video compression format, and is currently one of the most commonly used formats for the recording, compression, and distribution of video content.</source>
        <translation type="obsolete">H264 je jeden z kompresních formátů obrazu a je v současnosti jedním z nejpoužívanějších formátů pro nahrávání, komprese a šíření videoobsahu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="860"/>
        <source>This track can be renamed and must contain an extension to avoid reading errors.</source>
        <translation type="obsolete">tato stopa může být přejmenována a musí obsahovat příponu kvůli vyhnutí se chbám při čtení.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="782"/>
        <source> MKV File Tracks </source>
        <translation type="obsolete"> Stopy souboru MKV </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="796"/>
        <source>The SubRip file format is perhaps the most basic of all subtitle formats.</source>
        <translation type="obsolete">Souborový formát SRT (SubRip) je jistě nejjednodušším formátem titulků.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="680"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; file given as argument does not exist.</source>
        <translation type="obsolete">Soubor &lt;b&gt;{}&lt;/b&gt; zadaný jako argument neexistuje.</translation>
    </message>
    <message>
        <location filename="" line="731"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Rozhraní pro vytahování, úpravy a opětovné zabalení stop souboru matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Tento program následuje po několika dalších, jež byly programovány v bashi.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tento program je licencován pod &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;Creative Commons BY-NC-SA&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Velké poděkování fórům pro python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; za jejich trpělivost.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Vytvořeno &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), listopad 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1023"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ztratil jste se? Potřebujete pomoc?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Obvykle jsou přítomny všechny nutné informace:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Čtěte informace ve stavovém řádku, když přejíždíte ukazovátkem myši nad prvky rozhraní&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Pokud máte přesto potřebu ještě dalších informací:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Moje e-mailová adresa: &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Děkuji vám za zájem, který věnujete tomuto programu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="776"/>
        <source>Change the fps value if needed. Useful in case of audio lag. Normal : 23.976, 25.000 and 30.000.</source>
        <translation type="obsolete">V případě potřeby vybrat dobrý počet obrázků za sekundu. Užitečné v případě prodlevy zvuku. Normální: 23.976, 25.000 a 30.000.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="663"/>
        <source>This file is not supported by mkvmerge.
Do you want convert this file in mkv ?</source>
        <translation type="obsolete">Tento soubor není podporován mkvmerge.
Chcete tento soubor převést do mkv?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="951"/>
        <source>Not supported file</source>
        <translation type="obsolete">Není podporováno soubor</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="755"/>
        <source>MKV Merge Gui or MKV Extractor Qt ?</source>
        <translation type="obsolete">MKV Merge Gui nebo MKV Extractor Qt ?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1069"/>
        <source>You want extract and reencapsulate the tracks without use other options.

If you just need to make this, you should use MMG (MKV Merge gui) who is more adapted for this job.

What software do you want use ?</source>
        <translation type="obsolete">Vy chcete extrahovat a reencapsulate skladby bez použití dalších možností.

Pokud jste právě potřebujete, aby se to, měli byste použít MMG (MKV Merge GUI), který je více přizpůsoben pro tuto práci.

Jaký software chceš používat?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1053"/>
        <source>Select the output folder to use for convert the file</source>
        <translation type="obsolete">yberte výstupní složku použít pro převést soubor</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash and it codec in python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Rozhraní pro vytahování, úpravy a opětovné zabalení stop souboru matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Tento program následuje po několika dalších, jež byly programovány v Bash a to kodek v python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tento program je licencován pod &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;Creative Commons BY-NC-SA&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Velké poděkování fórům pro python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; za jejich trpělivost.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Vytvořeno &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), listopad 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="1048"/>
        <source>Use the same output folder as the MKV file</source>
        <translation type="obsolete">Použít stejnou výstupní složku, jako pro soubor MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="650"/>
        <source>Quit</source>
        <translation type="obsolete">Ukončit</translation>
    </message>
</context>
<context>
    <name>MKVExtractorQt5</name>
    <message>
        <location filename="MKVExtractorQt5.py" line="642"/>
        <source>About MKV Extractor Gui</source>
        <translation>O rozhraní MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="643"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;GUI to extract/edit/remux the tracks of a matroska (MKV) file.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This program follows several others that were coded in Bash and it codec in python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This software is licensed under &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;GNU GPL v3&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Thanks to the &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; python forums for their patience&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), November 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MKV Extractor Qt v{}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Rozhraní pro vytahování, úpravy a opětovné zabalení stop souboru matroska (MKV).&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Tento program následuje po několika dalších, jež byly programovány v Bash a to kodek v python3 + QT5.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tento program je licencován pod &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;&lt;a href=&quot;{}&quot;&gt;Creative Commons BY-NC-SA&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Velké poděkování fórům pro python &lt;a href=&quot;http://www.developpez.net/forums/f96/autres-langages/python-zope/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;developpez.net&lt;/span&gt;&lt;/a&gt; za jejich trpělivost.&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Vytvořeno &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; (Hizoka), listopad 2013&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="649"/>
        <source>They talk about MKV Extractor Gui</source>
        <translation>Mluví o MKV Extractor Gui</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="650"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://sysads.co.uk/2014/09/install-mkv-extractor-qt-5-1-4-ubuntu-14-04/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;sysads.co.uk&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.softpedia.com/reviews/linux/mkv-extractor-qt-review-496919.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linux.softpedia.com/get/Multimedia/Video/MKV-Extractor-Qt-103555.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;linux.softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://zenway.ru/page/mkv-extractor-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;zenway.ru&lt;/span&gt;&lt;/a&gt; (Russian)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linuxg.net/how-to-install-mkv-extractor-qt-5-1-4-on-ubuntu-14-04-linux-mint-17-elementary-os-0-3-deepin-2014-and-other-ubuntu-14-04-derivatives/&quot;&gt;linuxg.net&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://la-vache-libre.org/mkv-extractor-gui-virer-les-sous-titres-inutiles-de-vos-fichiers-mkv-et-plus-encore/&quot;&gt;la-vache-libre.org&lt;/span&gt;&lt;/a&gt; (French)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://passionexubuntu.altervista.org/index.php/it/kubuntu/1152-mkv-extractor-qt-vs-5-1-3-kde.html&quot;&gt;passionexubuntu.altervista.org&lt;/span&gt;&lt;/a&gt; (Italian)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://sysads.co.uk/2014/09/install-mkv-extractor-qt-5-1-4-ubuntu-14-04/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;sysads.co.uk&lt;/span&gt;&lt;/a&gt; (angličtina)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.softpedia.com/reviews/linux/mkv-extractor-qt-review-496919.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linux.softpedia.com/get/Multimedia/Video/MKV-Extractor-Qt-103555.shtml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;linux.softpedia.com&lt;/span&gt;&lt;/a&gt; (English)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://zenway.ru/page/mkv-extractor-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;zenway.ru&lt;/span&gt;&lt;/a&gt; (Russian)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://linuxg.net/how-to-install-mkv-extractor-qt-5-1-4-on-ubuntu-14-04-linux-mint-17-elementary-os-0-3-deepin-2014-and-other-ubuntu-14-04-derivatives/&quot;&gt;linuxg.net&lt;/span&gt;&lt;/a&gt; (angličtina)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://la-vache-libre.org/mkv-extractor-gui-virer-les-sous-titres-inutiles-de-vos-fichiers-mkv-et-plus-encore/&quot;&gt;la-vache-libre.org&lt;/span&gt;&lt;/a&gt; (francouzština)&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://passionexubuntu.altervista.org/index.php/it/kubuntu/1152-mkv-extractor-qt-vs-5-1-3-kde.html&quot;&gt;passionexubuntu.altervista.org&lt;/span&gt;&lt;/a&gt; (Italian)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="652"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="653"/>
        <source>The command(s) have finished</source>
        <translation>Příkaz(y) byl dokončen(y)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="654"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; command have finished its work.</source>
        <translation>Příkazy &lt;b&gt;{}&lt;/b&gt; dokončil svoji práci.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="655"/>
        <source>All commands have finished their work.</source>
        <translation>Všechny příkazy dokončily svoji práci.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="657"/>
        <source>Use the right click for view options.</source>
        <translation>Použijte klepnutí pravým tlačítkem myši pro zobrazení voleb.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="659"/>
        <source>All compatible Files</source>
        <translation>Všechny nevylučující se soubory</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="660"/>
        <source>Matroska Files</source>
        <translation>Soubory Matroska</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="661"/>
        <source>Other Files</source>
        <translation>Jiné soubory</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="663"/>
        <source>Do not ask again</source>
        <translation>Neptat se znovu</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="664"/>
        <source>File needs to be converted</source>
        <translation>Soubor je třeba převést</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="665"/>
        <source>This file is not supported by mkvmerge.
Do you want convert this file in mkv ?</source>
        <translation>Tento soubor není podporován mkvmerge.
Chcete tento soubor převést do mkv?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="666"/>
        <source>MKVMerge Warning</source>
        <translation>Varování MKVMerge</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="667"/>
        <source>A warning has occurred during the convertion of the file, read the feedback informations.</source>
        <translation>Během převádění souboru se objevilo varování. Čtěte zpětnovazební informace.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="668"/>
        <source>Do not warn me</source>
        <translation>Nevarovat</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="669"/>
        <source>Choose the out folder of the new mkv file</source>
        <translation>Vyberte výstupní složku pro nový soubor mkv</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="671"/>
        <source>Already existing file</source>
        <translation>Soubor již existuje</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="672"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; is already existing, overwrite it?</source>
        <translation>&lt;b&gt;{}&lt;/b&gt; již existuje. Přepsat jej?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="674"/>
        <source>Awaiting resume</source>
        <translation>Čeká se na pokračování</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="675"/>
        <source>The software is &lt;b&gt;pausing&lt;/b&gt;.&lt;br/&gt;Thanks to clic on the &apos;&lt;b&gt;Resume work&lt;/b&gt;&apos; button or &apos;&lt;b&gt;Cancel work&lt;/b&gt;&apos; for cancel all the work and remove the temporary files.</source>
        <translation>Program je &lt;b&gt;pozastaven&lt;/b&gt;.&lt;br/&gt;Klepněte na tlačítko &apos;&lt;b&gt;Pokračovat v práci&lt;/b&gt;&apos; nebo na tlačítko &apos;&lt;b&gt;Zrušit práci&lt;/b&gt;&apos; pro zrušení vší práce a odstranění dočasných souborů.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="676"/>
        <source>Resume work</source>
        <translation>Pokračovat v práci</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="677"/>
        <source>Cancel work</source>
        <translation>Zrušit práci</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="679"/>
        <source>The last file doesn&apos;t exist</source>
        <translation>Poslední soubor neexistuje</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="680"/>
        <source>You have checked the option who reload the last file to the launch of MKV Extractor Qt, but this last file doesn&apos;t exist anymore.</source>
        <translation>Zaškrtnul jste volbu, která znovunahraje poslední soubor při spuštění MKV Extractor Qt, ale tento soubor už neexistuje.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="681"/>
        <source>Wrong arguments</source>
        <translation>Špatné argumenty</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="682"/>
        <source>The &lt;b&gt;{}&lt;/b&gt; file given as argument does not exist.</source>
        <translation>Soubor &lt;b&gt;{}&lt;/b&gt; zadaný jako argument neexistuje.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="683"/>
        <source>&lt;b&gt;Too many arguments given:&lt;/b&gt;&lt;br/&gt; - {} </source>
        <translation>&lt;b&gt;Zadáno příliš mnoho argumentů:&lt;/b&gt;&lt;br/&gt; - {} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="684"/>
        <source>Wrong value</source>
        <translation>Nesprávná hodnota</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="685"/>
        <source>Wrong value for the &lt;b&gt;{}&lt;/b&gt; option, MKV Extractor Qt will use the default value.</source>
        <translation>Nesprávná hodnota pro volbu &lt;b&gt;{}&lt;/b&gt;, MKV Extractor Qt použije výchozí hodnotu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="686"/>
        <source>Wrong path for the &lt;b&gt;{}&lt;/b&gt; option, MKV Extractor Qt will use the default path.</source>
        <translation>Nesprávná cesta pro volbu &lt;b&gt;{}&lt;/b&gt;, MKV Extractor Qt použije výchozí cestu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="687"/>
        <source>No way to open this file</source>
        <translation>Soubor nelze otevřít</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="688"/>
        <source>The file to open contains quotes (&quot;) in its name. It&apos;s impossible to open a file with this carac. Please rename it.</source>
        <translation>Soubor k otevření obsahuje ve svém názvu uvozovky (&quot;). Soubor s tímto znakem nelze otevřít. Přejmenujte jej, prosím.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="689"/>
        <source>Space available</source>
        <translation>Dostupné místo</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="690"/>
        <source>Not enough space available in the &lt;b&gt;{}&lt;/b&gt; folder.&lt;br/&gt;It is advisable to have at least twice the size of free space on the disk file.&lt;br&gt;Free disk space: &lt;b&gt;{}&lt;/b&gt;.&lt;br&gt;File size: &lt;b&gt;{}&lt;/b&gt;.</source>
        <translation>Nedostatek dostupného místa ve složce &lt;b&gt;{}&lt;/b&gt;.&lt;br/&gt; Doporučuje se mít na disku volného místa alespoň dvojnásobek velikosti souboru.&lt;br&gt;Uvolněte místo na disku: &lt;b&gt;{}&lt;/b&gt;.&lt;br&gt;Velikost souboru: &lt;b&gt;{}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="691"/>
        <source>Not enough space available in &lt;b&gt;{}&lt;/b&gt; folder.&lt;br/&gt;Free space in the disk: &lt;b&gt;{}&lt;/b&gt;&lt;br/&gt;File size: &lt;b&gt;{}&lt;/b&gt;.</source>
        <translation>Nedostatek dostupného místa ve složce &lt;b&gt;{}&lt;/b&gt;.&lt;br/&gt; Uvolněte místo na disku: &lt;b&gt;{}&lt;/b&gt;&lt;br&gt;Velikost souboru: &lt;b&gt;{}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="693"/>
        <source>Help me!</source>
        <translation>Pomozte!</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="694"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are you lost? Do you need help? &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Normally all necessary information is present: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Read the information in the status bar when moving the mouse on widgets &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Though, if you need more information: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=1508741&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- My email address: &lt;a href=&quot;mailto:hizo@free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Thank you for your interest in this program.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ztratil jste se? Potřebujete pomoci?&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Obvykle jsou přítomny všechny nutné informace:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Čtěte informace ve stavovém řádku, když přejíždíte ukazovátkem myši nad prvky rozhraní&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Pokud přesto máte potřebu ještě dalších informací:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Forum Ubuntu-fr.org: &lt;a href=&quot;http://forum.ubuntu-fr.org/viewtopic.php?id=293216&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;topic&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;- Moje e-mailová adresa: &lt;a href=&quot;mailto:hizo@free.fr &quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;hizo@free.fr &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Děkuji vám za zájem, který věnujete tomuto programu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="696"/>
        <source>Skip the existing file test.</source>
        <translation>Přeskočit ověření existence souboru.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="697"/>
        <source>Quality of the ac3 file converted.</source>
        <translation>Jakost převedeného souboru ac3.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="698"/>
        <source>Power of the ac3 file converted.</source>
        <translation>Výkon převedeného souboru ac3.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="699"/>
        <source>Skip the free space disk test.</source>
        <translation>Přeskočit ověření volného prostoru.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="700"/>
        <source>View more informations in feedback box.</source>
        <translation>Zobrazit více informací v okně s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="701"/>
        <source>Delete temporary files.</source>
        <translation>Smazat dočasné soubory.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="702"/>
        <source>Remove the error message if the last file doesn&apos;t exist.</source>
        <translation>Odstranit tuto chybovou zprávu, pokud už poslední soubor neexistuje.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="703"/>
        <source>Show or hide the information feedback box.</source>
        <translation>Ukázat nebo skrýt okno s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="704"/>
        <source>Anchor or loose information feedback box.</source>
        <translation>Ukotvit nebo odpojit okno s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="705"/>
        <source>The folder to use for extract temporaly the attachements file to view them.</source>
        <translation>Složka, která se má použít pro dočasné vytažení připojených souborů a jejich zobrazení.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="730"/>
        <source>Use FFMpeg for the conversion.</source>
        <translation>Použít FFMpeg pro převod.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="707"/>
        <source>Keep in memory the last file opened for open it at the next launch of MKV Extractor Qt.</source>
        <translation>Uchovat v paměti naposledy otevřený soubor pro jeho otevření při příštím spuštění MKV Extractor Qt.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="708"/>
        <source>Software to use for just encapsulate.</source>
        <translation>Program pro jednoduché zabalení.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="709"/>
        <source>Skip the proposal to softaware to use.</source>
        <translation>Přeskočit návrh programu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="710"/>
        <source>Skip the confirmation of the conversion.</source>
        <translation>Přeskočit potvrzení převodu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="711"/>
        <source>Hide the information of the conversion warning.</source>
        <translation>Skrýt upozornění na převod.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="712"/>
        <source>Folder of the MKV files.</source>
        <translation>Složka se soubory MKV.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="713"/>
        <source>Output folder for the new MKV files.</source>
        <translation>Výstupní složka pro soubory MKV.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="714"/>
        <source>Software language to use.</source>
        <translation>Jazyk programu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="715"/>
        <source>Remove the Qt file who keeps the list of the recent files for the window selection.</source>
        <translation>Odstranit soubor Qt, který uchovává seznam nedávných souborů výběrového okna.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="716"/>
        <source>Use the same input and output folder.</source>
        <translation>Použít stejnou vstupní a výstupní složku.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="717"/>
        <source>Automatically rename the output file name in MEG_FileName.</source>
        <translation>Automaticky přejmenovat název výstupního souboru na MEG_NázevSouboru.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="718"/>
        <source>Switch to stereo during conversion.</source>
        <translation>Během převodu přepnout do sterea.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="719"/>
        <source>Opening subtitles before encapsulation.</source>
        <translation>Otevřít titulky před zabalením.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="720"/>
        <source>Display or hide the system tray icon.</source>
        <translation>Zobrazit nebo skrýt ikonu programu v oznamovací oblasti panelu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="721"/>
        <source>Number of CPU to use with Tesseract, by default: max value.</source>
        <translation>Výchozí počet procesorů k použití s Tesseract: nejvyšší hodnota.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="722"/>
        <source>Keep in memory the aspect and the position of the window for the next opened.</source>
        <translation>Uchovat v paměti velikost okna a jeho polohu pro jeho příští otevření.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="724"/>
        <source>Number of CPU to use</source>
        <translation>Počet procesorů</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="725"/>
        <source>Choose the number of CPU to use with Tesseract.</source>
        <translation>Vyberte počet procesorů k použití s Tesseract.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="726"/>
        <source>Convert in AC3</source>
        <translation>Převod do AC3</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="727"/>
        <source>Convert audio tracks automatically to AC3.</source>
        <translation>Převést automaticky zvukové stopy do AC3.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="728"/>
        <source>Delete temporary files</source>
        <translation>Smazat dočasné soubory</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="729"/>
        <source>The temporary files are the extracted tracks.</source>
        <translation>Dočasné soubory jsou vytažené stopy.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="731"/>
        <source>Increase the sound power</source>
        <translation>Zvětšit zvukový výkon</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="732"/>
        <source>No power change.</source>
        <translation>Žádná změna výkonu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="733"/>
        <source>Multiplying audio power by {}.</source>
        <translation>Znásobení zvukového výkonu {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="734"/>
        <source>Power x {}</source>
        <translation>Výkon x {}</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="735"/>
        <source>List of available flow rates of conversion</source>
        <translation>Seznam dostupných převodních datových toků</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="736"/>
        <source>Convert the audio quality in {} kbits/s.</source>
        <translation>Převede jakost u zvuku na {} kbits/s.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="737"/>
        <source>{} kbits/s</source>
        <translation>{} kbits/s</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="738"/>
        <source>Switch to stereo during conversion</source>
        <translation>Během převodu přepnout do sterea</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="739"/>
        <source>The audio will not use the same number of channels, the audio will be stereo (2 channels).</source>
        <translation>Zvuk nezachová stejný počet kanálů. Zvuk bude stereo (2 kanály).</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="740"/>
        <source>Opening subtitles before encapsulation</source>
        <translation>Otevřít titulky před zabalením</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="741"/>
        <source>Auto opening of subtitle srt files for correction. The software will be paused.</source>
        <translation>Otevřít automaticky soubory titulků srt pro jejich opravení. Tento program bude pozastaven.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="742"/>
        <source>New value for &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; option: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation>Nová hodnota pro &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt; : &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="743"/>
        <source>No change the quality</source>
        <translation>Žádná změna jakosti</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="744"/>
        <source>The quality of the audio tracks will not be changed.</source>
        <translation>Jakost zvukové stopy nebude měněna.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="745"/>
        <source>No change the power</source>
        <translation>Žádná změna výkonu</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="746"/>
        <source>The power of the audio tracks will not be changed.</source>
        <translation>Výkon zvukových stop nebude měněn.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="748"/>
        <source>Selected file: {}.</source>
        <translation>Vybraný soubor: {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="749"/>
        <source>Selected folder: {}.</source>
        <translation>Vybraná složka: {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="750"/>
        <source>Always use the same output folder as the input MKV file (automatically updated)</source>
        <translation>Vždy použít stejnou výstupní složku jako vstupní soubor MKV (aktualizováno automaticky)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="752"/>
        <source>Keep in memory the last file opened for open it at the next launch of MKV Extractor Qt (to use for tests)</source>
        <translation>Uchovat v paměti naposledy otevřený soubor pro jeho otevření při příštím spuštění MKV Extractor Qt (k užití při zkoušení)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="753"/>
        <source>Select the input MKV File</source>
        <translation>Vybrat vstupní soubor MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="754"/>
        <source>Select the output MKV file</source>
        <translation>Vybrat výstupní soubor MKV</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="755"/>
        <source>Select the output folder</source>
        <translation>Vybrat výstupní složku</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="757"/>
        <source>MKV Merge Gui or MKV Extractor Qt ?</source>
        <translation>MKV Merge Gui nebo MKV Extractor Qt?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="758"/>
        <source>You want extract and reencapsulate the tracks without use other options.

If you just need to make this, you should use MMG (MKV Merge gui) who is more adapted for this job.

What software do you want use ?
</source>
        <translation>Chcete vytáhnout a znovuzabalit stopy bez použití dalších voleb.

Pokud potřebujete udělat jen toto, měli byste použít MMG (MKV Merge GUI), který je k této práci více uzpůsoben.

Jaký software chceš používat?</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="760"/>
        <source>Always use the default file rename (MEG_FileName)</source>
        <translation>Vždy použít výchozí způsob přejmenování (MEG_NázevSouboru)</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="761"/>
        <source>Choose the output file name</source>
        <translation>Vybrat název výstupního souboru</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="763"/>
        <source>audio</source>
        <translation>Zvuk</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="764"/>
        <source>subtitles</source>
        <translation>Titulky</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="765"/>
        <source>video</source>
        <translation>Obraz</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="767"/>
        <source>If the remuxed file has reading problems, change this value.</source>
        <translation>Pokud má nově zabalený soubor potíže se čtením, změňte tuto hodnotu.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="768"/>
        <source>Change the language if it&apos;s not right. &apos;und&apos; means &apos;Undetermined&apos;.</source>
        <translation>Měnit jazyk, pokud není správný. &apos;und&apos; znamená &apos;nestanovený&apos;.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="769"/>
        <source>This track can be renamed and must contain an extension to avoid reading errors by doubleclicking.</source>
        <translation>Tato stopa může být přejmenována a musí obsahovat příponu kvůli vyhnutí se chybám při čtení pomocí dvojitého klepnutí.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="770"/>
        <source>chapters</source>
        <translation>Kapitoly</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="771"/>
        <source>Work with track number {}.</source>
        <translation>Pracovat se stopou číslo {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="772"/>
        <source>Work with attachment number {}.</source>
        <translation>Pracovat na připojeném souboru číslo {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="773"/>
        <source>Work with {}.</source>
        <translation>Pracovat na {}.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="774"/>
        <source>This track can be renamed by doubleclicking.</source>
        <translation>Tato stopa může být přejmenována pomocí dvojitého klepnutí.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="775"/>
        <source>tags</source>
        <translation>Značky</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="776"/>
        <source>This track is a {} type and cannot be previewed.</source>
        <translation>Tato stopa je typu {} a nelze ji zobrazit.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="777"/>
        <source>This attachment file is a {} type, it can be extracted (speedy) and viewed by clicking.</source>
        <translation>Tento připojený soubor je typu {}. Lze jej vytáhnout (rychlý) a zobrazit pomocí klepnutí.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="778"/>
        <source>Change the fps value if needed. Useful in case of audio lag. Normal : 23.976, 25.000 and 30.000.</source>
        <translation>V případě potřeby vybrat dobrý počet obrázků za sekundu. Užitečné v případě prodlevy zvuku. Normální: 23.976, 25.000 a 30.000.</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="780"/>
        <source> All commands were canceled </source>
        <translation> Všechny příkazy byly zrušeny </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="781"/>
        <source>Command execution: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</source>
        <translation>Provedení příkazu: &lt;span style=&quot; color:#0000c0;&quot;&gt;{}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="782"/>
        <source> The last command returned an error </source>
        <translation> Poslední příkaz vrátil chybu </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="783"/>
        <source> {} execution is finished </source>
        <translation> Provedení {} je dokončeno </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="784"/>
        <source> MKV File Tracks </source>
        <translation> Stopy souboru MKV </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="785"/>
        <source> {} execution in progress </source>
        <translation> Probíhá provedení {} </translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="645"/>
        <source>About Qtesseract5</source>
        <translation>O programu Qtesseract5</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="646"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;b&gt;Qtesseract5&lt;/b&gt; is a software who converts the IDX/SUB file in SRT (text) file. For that works, it use &lt;i&gt;subp2pgm&lt;/i&gt; (export the images files from SUB file), &lt;i&gt;Tesseract&lt;/i&gt; (for read their files) and &lt;i&gt;subptools&lt;/i&gt; (to create a SRT file).&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://forum.ubuntu-fr.org/viewtopic.php?pid=21507283&quot;&gt;Topic on ubuntu-fr.org&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Created by &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguic Terence&lt;/span&gt; &lt;hizo@free.fr&gt;, April 2016&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;b&gt;Qtesseract5&lt;/b&gt; je program, který převádí soubor IDX/SUB na soubor SRT (text). Na tuto práci používá &lt;i&gt;subp2pgm&lt;/i&gt; (vyvedení obrázků ze souboru SUB), &lt;i&gt;Tesseract&lt;/i&gt; (ke čtení jejich souborů) a &lt;i&gt;subptools&lt;/i&gt; (k vytvoření souboru SRT).&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://forum.ubuntu-fr.org/viewtopic.php?pid=21507283&quot;&gt;Téma na ubuntu-fr.org&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Vytvořeno &lt;span style=&quot; font-weight:600;&quot;&gt;Belleguicem Terencem&lt;/span&gt; &lt;hizo@free.fr&gt;, duben 2016&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QTextEditCustom</name>
    <message>
        <location filename="MKVExtractorQt5.py" line="80"/>
        <source>Clean the information fee&amp;dback box</source>
        <translation>Vyčistit okno s &amp;informacemi</translation>
    </message>
    <message>
        <location filename="MKVExtractorQt5.py" line="87"/>
        <source>&amp;Export info to ~/InfoMKVExtractorQt5.txt</source>
        <translation>Vyvedení informací do souboru ~/InfoMKVExtractorQt5.txt</translation>
    </message>
</context>
<context>
    <name>mkv_extractor_qt</name>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="877"/>
        <source>File</source>
        <translation type="obsolete">Soubor</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="910"/>
        <source>Help</source>
        <translation type="obsolete">Nápověda</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="537"/>
        <source>Next</source>
        <translation type="obsolete">Další</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1059"/>
        <source>Quit</source>
        <translation type="obsolete">Ukončit</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="772"/>
        <source>Stop</source>
        <translation type="obsolete">Zastavit</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1041"/>
        <source>Output folder</source>
        <translation type="obsolete">Výstupní složka</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="283"/>
        <source>Codec</source>
        <translation type="obsolete">Kodek</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="305"/>
        <source>Remux</source>
        <translation type="obsolete">Zabalit znovu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1080"/>
        <source>View MKV information</source>
        <translation type="obsolete">Zobrazit informace o MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1023"/>
        <source>Open an MKV file</source>
        <translation type="obsolete">Otevřít soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1026"/>
        <source>Chosing an MKV file is required.</source>
        <translation type="obsolete">Výběr souboru MKV je povinný.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="267"/>
        <source>Name/Information</source>
        <translation type="obsolete">Název/Informace</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1059"/>
        <source>About MKV Extractor Qt</source>
        <translation type="obsolete">O programu MKV Extractor Qt</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="400"/>
        <source>Subtitles recognized:</source>
        <translation type="obsolete">Rozpoznání titulků:</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1245"/>
        <source>Open the MKV file with the default application.</source>
        <translation type="obsolete">Otevřít soubor MKV s výchozím programem.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="534"/>
        <source>Next subtitle image.</source>
        <translation type="obsolete">Další obrázek titulků.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="369"/>
        <source>Convert DTS tracks automatically to AC3, which is much more compatible. Options are available.</source>
        <translation type="obsolete">Převést automaticky stopy DTS do AC3, což je formát čitelný mnohými nástroji. Volby jsou dostupné.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1044"/>
        <source>Chosing an output folder is mandatory if the option to use the same folder as the MKV file is not used.</source>
        <translation type="obsolete">Výběr výstupního adresáře je povinný, pokud volba používající tutéž složku jako soubor MKV není používána.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="ui_MKVExtractorQt5.ui" line="972"/>
        <source>Traduire le logiciel en langue Française.</source>
        <translation type="obsolete">Překlad programu do francouzštiny.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1173"/>
        <source>Display information about Qt.</source>
        <translation type="obsolete">Zobrazit informace o Qt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1044"/>
        <source>Optimize MKV file</source>
        <translation type="obsolete">Vyladit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1044"/>
        <source>Change the display language to English.</source>
        <translation type="obsolete">Změnit jazyk rozhraní na angličtinu.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1062"/>
        <source>Quit the program and save the options.</source>
        <translation type="obsolete">Ukončit program a uložit volby.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="804"/>
        <source>Resume</source>
        <translation type="obsolete">Pokračovat</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1224"/>
        <source>Open the MKV file with mkvmerge GUI for more modifications.</source>
        <translation type="obsolete">Otevřít soubor MKV s rozhraním MKV Merge pro více úprav.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="916"/>
        <source>Options</source>
        <translation type="obsolete">Volby</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1424"/>
        <source>Use avconv</source>
        <translation type="obsolete">Použít avconv</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1432"/>
        <source>Use ffmpeg</source>
        <translation type="obsolete">Použít ffmpeg</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="769"/>
        <source>Cancel running processes and delete resulting file.</source>
        <translation type="obsolete">Zrušit běžící procesy a smazat výsledný soubor.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1194"/>
        <source>Change the output folder automatically to use the same folder as the MKV file.</source>
        <translation type="obsolete">Změnit výstupní složku automaticky pro použití téže složky, jakou používá soubor MKV.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="130"/>
        <source>Available tracks in MKV file:</source>
        <translation type="obsolete">Dostupné stopy v souboru MKV:</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="876"/>
        <source>Actions</source>
        <translation type="obsolete">Činnosti</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1275"/>
        <source>Reset the information feedback box.</source>
        <translation type="obsolete">Nastavit znovu pole se zpětnou informační vazbou.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1170"/>
        <source>Replay MKV file</source>
        <translation type="obsolete">Číst soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="331"/>
        <source>SUB to SRT</source>
        <translation type="obsolete">Ze SUB na SRT</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="372"/>
        <source>DTS to AC3</source>
        <translation type="obsolete">Z DTS na AC3</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="164"/>
        <source>MKV Title</source>
        <translation type="obsolete">Název MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1101"/>
        <source>Verify Matroska files for specification conformance with MKValidator.</source>
        <translation type="obsolete">Ověřit soubory Matroska kvůli souladu specifikace s MKValidator.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="489"/>
        <source>Previous</source>
        <translation type="obsolete">Předchozí</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1042"/>
        <source>Information feedback:</source>
        <translation type="obsolete">Zpětná informační vazba:</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1098"/>
        <source>About Qt</source>
        <translation type="obsolete">O Qt</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1083"/>
        <source>Launch extract/convert/mux tracks.</source>
        <translation type="obsolete">Spustit vytahování/převod/balení stop.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="302"/>
        <source>Automatically remux extracted tracks into a new MKV file. Options are available.</source>
        <translation type="obsolete">Automaticky zabalit znovu vytažené stopy do nového souboru MKV. Volby jsou dostupné.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="801"/>
        <source>Press button to resume.</source>
        <translation type="obsolete">Stiskněte tlačítko pro pokračování.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1155"/>
        <source>Display information about the MKV file with mkvinfo.</source>
        <translation type="obsolete">Zobrazit informace o souboru MKV a MKVInfo.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1119"/>
        <source>Clean/Optimize Matroska files with MKClean.</source>
        <translation type="obsolete">Vyčistit/Vyladit soubor Matroska, který je již zabalen pomocí MKClean.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="ui_MKVExtractorQt5.ui" line="1047"/>
        <source>Version Française</source>
        <translation type="obsolete">Francouzská verze</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1149"/>
        <source>Edit MKV file</source>
        <translation type="obsolete">Upravit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="155"/>
        <source>The title of the MKV file. This information can be edited. </source>
        <translation type="obsolete">Název souboru MKV. Tato informace může být změněna.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1185"/>
        <source>Need help</source>
        <translation type="obsolete">Potřeba pomoci</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1185"/>
        <source>Save MKV filename</source>
        <translation type="obsolete">Uložit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="428"/>
        <source>Tesseract couldn&apos;t recognize the subtitle file. It must be done manually.</source>
        <translation type="obsolete">Tesseractu se nepodařilo rozpoznat soubor s titulky. Je to třeba udělat ručně.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="990"/>
        <source>Hide / Show the information feedback box.</source>
        <translation type="obsolete">Skrýt/Ukázat okno s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="349"/>
        <source>Automatic conversion from vobsub to SRT.</source>
        <translation type="obsolete">Automatický převod titulků z vobsub na srt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1134"/>
        <source>Delete config file</source>
        <translation type="obsolete">Smazat soubor s nastavením</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="275"/>
        <source>Language/fps/Size</source>
        <translation type="obsolete">Jazyk/FPS/Velikost</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="743"/>
        <source>Execute</source>
        <translation type="obsolete">Provést</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="486"/>
        <source>Previous subtitle image.</source>
        <translation type="obsolete">Předchozí obrázek titulků.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1008"/>
        <source>Start job queue</source>
        <translation type="obsolete">Spustit řadu úkolů k provedení</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1119"/>
        <source>Use the same output folder as the MKV file</source>
        <translation type="obsolete">Použít stejnou výstupní složku, jako pro soubor MKV</translation>
    </message>
    <message>
        <location filename="" line="111"/>
        <source>Keep the MKV file in memory for the next session.</source>
        <translation type="obsolete">Ponechat soubor MKV v paměti pro příští spuštění programu.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1209"/>
        <source>Delete config file and don&apos;t save the new values for this time.</source>
        <translation type="obsolete">Smazat soubor s nastavením a neukládat pro tentokrát nové hodnoty.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1197"/>
        <source>Clean the information feedback box</source>
        <translation type="obsolete">Vyčistit okno s informacemi</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1026"/>
        <source>Check MKV file</source>
        <translation type="obsolete">Ověřit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1113"/>
        <source>Export info to ~/InfoMKVExtractorQt.txt.</source>
        <translation type="obsolete">Vyvedení informací do souboru ~/InfoMKVExtractorQt.txt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1134"/>
        <source>Display information about MKV Extractor Qt.</source>
        <translation type="obsolete">Otevřít okno s informacemi o programu.</translation>
    </message>
</context>
<context>
    <name>mkv_extractor_qt5</name>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="130"/>
        <source>Available tracks in MKV file:</source>
        <translation>Dostupné stopy v souboru MKV:</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="155"/>
        <source>The title of the MKV file. This information can be edited. </source>
        <translation>Název souboru MKV. Tato informace může být změněna.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="164"/>
        <source>MKV Title</source>
        <translation>Název MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="267"/>
        <source>Name/Information</source>
        <translation>Název/Informace</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="275"/>
        <source>Language/fps/Size</source>
        <translation>Jazyk/Snímků za sekundu/Velikost</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="283"/>
        <source>Codec</source>
        <translation>Kodek</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="302"/>
        <source>Automatically remux extracted tracks into a new MKV file. Options are available.</source>
        <translation>Vytažené stopy automaticky znovuzabalit do nového souboru MKV. Volby jsou dostupné.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="305"/>
        <source>Remux</source>
        <translation>Zabalit znovu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="328"/>
        <source>Convertion subtitles option.</source>
        <translation>Volba pro převod titulků.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="331"/>
        <source>SUB to SRT</source>
        <translation>Ze SUB na SRT</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="354"/>
        <source>Convertion audio option.</source>
        <translation>Volba pro převod zvuku.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="357"/>
        <source>Audio Convert</source>
        <translation>Převod zvuku</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="400"/>
        <source>Subtitles recognized:</source>
        <translation>Rozpoznání titulků:</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="428"/>
        <source>Tesseract couldn&apos;t recognize the subtitle file. It must be done manually.</source>
        <translation>Tesseractu se nepodařilo rozpoznat soubor s titulky. Je třeba to udělat ručně.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="486"/>
        <source>Previous subtitle image.</source>
        <translation>Předchozí obrázek s titulky.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="489"/>
        <source>Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="534"/>
        <source>Next subtitle image.</source>
        <translation>Další obrázek s titulky.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="537"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="573"/>
        <source>Configuration  edition</source>
        <translation>Úprava nastavení</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="917"/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="626"/>
        <source>Values</source>
        <translation>Hodnoty</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="631"/>
        <source>Default Values</source>
        <translation>Výchozí hodnoty</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="650"/>
        <source>&amp;Reset</source>
        <translation>&amp;Nastavit znovu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="674"/>
        <source>Delete the actual value will use the default value</source>
        <translation>Po smazání nynější hodnoty se použije výchozí hodnota</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="700"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1084"/>
        <source>Launch extract/convert/mux tracks.</source>
        <translation>Spustit vytahování/převod/balení stop.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="743"/>
        <source>Execute</source>
        <translation>Provést</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="769"/>
        <source>Cancel running processes and delete resulting file.</source>
        <translation>Zrušit běžící procesy a smazat výsledný soubor.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="772"/>
        <source>Stop</source>
        <translation>Zastavit</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="789"/>
        <source>Pause</source>
        <translation>Pozastavit</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1063"/>
        <source>Quit the program and save the options.</source>
        <translation>Ukončit program a uložit volby.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1060"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="866"/>
        <source>Fi&amp;le</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="876"/>
        <source>Actions</source>
        <translation>Činnosti</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="899"/>
        <source>&amp;Help</source>
        <translation>Nápo&amp;věda</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="972"/>
        <source>&amp;Information feedback:</source>
        <translation>&amp;Zpětná vazba:</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1018"/>
        <source>&amp;Open an MKV file</source>
        <translation>&amp;Otevřít soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1024"/>
        <source>Open an MKV file</source>
        <translation>Otevřít soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1027"/>
        <source>Chosing an MKV file is required.</source>
        <translation>Výběr souboru MKV je povinný.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1039"/>
        <source>Output &amp;folder</source>
        <translation>Výstupní &amp;složka</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1042"/>
        <source>Output folder</source>
        <translation>Výstupní složka</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1045"/>
        <source>Chosing an output folder is mandatory if the option to use the same folder as the MKV file is not used.</source>
        <translation>Výběr výstupní složky je povinný, pokud není používána volba používající tutéž složku jako soubor MKV.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1057"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončit</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1081"/>
        <source>&amp;Start job queue</source>
        <translation>&amp;Spustit pracovní řadu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1099"/>
        <source>&amp;Check MKV file</source>
        <translation>&amp;Ověřit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1102"/>
        <source>Verify Matroska files for specification conformance with MKValidator.</source>
        <translation>Ověřit soubory Matroska kvůli souladu specifikace s MKValidator.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1117"/>
        <source>&amp;Optimize MKV file</source>
        <translation>&amp;Vyladit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1120"/>
        <source>Clean/Optimize Matroska files with MKClean.</source>
        <translation>Vyčistit/Vyladit soubor Matroska, který je již zabalen pomocí MKClean.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1132"/>
        <source>&amp;About MKV Extractor Qt</source>
        <translation>&amp;O programu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1135"/>
        <source>Display information about MKV Extractor Qt.</source>
        <translation type="obsolete">Otevřít okno s informacemi o programu.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1153"/>
        <source>&amp;View MKV information</source>
        <translation>&amp;Zobrazit informace o MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1156"/>
        <source>Display information about the MKV file with mkvinfo.</source>
        <translation>Zobrazit informace o souboru MKV a MKVInfo.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1171"/>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1174"/>
        <source>Display information about Qt.</source>
        <translation>Zobrazit informace o Qt.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1192"/>
        <source>&amp;Use the same output folder as the MKV file</source>
        <translation>&amp;Použít stejnou výstupní složku, jako pro soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1195"/>
        <source>Change the output folder automatically to use the same folder as the MKV file.</source>
        <translation>Změnit výstupní složku automaticky na tutéž složku, jakou používá soubor MKV.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1207"/>
        <source>C&amp;onfiguration  edition</source>
        <translation>Úprava &amp;nastavení</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1210"/>
        <source>Delete config file and don&apos;t save the new values for this time.</source>
        <translation>Smazat soubor s nastavením a neukládat pro tentokrát nové hodnoty.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1222"/>
        <source>&amp;Edit MKV file</source>
        <translation>&amp;Upravit soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1225"/>
        <source>Open the MKV file with mkvmerge GUI for more modifications.</source>
        <translation>Otevřít soubor MKV s rozhraním MKV Merge pro více úprav.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1243"/>
        <source>&amp;Replay MKV file</source>
        <translation>Čís&amp;t soubor MKV</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1246"/>
        <source>Open the MKV file with the default application.</source>
        <translation>Otevřít soubor MKV s výchozím programem.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1258"/>
        <source>&amp;Need help</source>
        <translation>&amp;Potřeba pomoci</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1261"/>
        <source>Help me! I&apos;m lost :(</source>
        <translation>Pomoc. Jsem ztracen</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1273"/>
        <source>&amp;Clean the information feedback box</source>
        <translation>&amp;Vyčistit okno s informacemi</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1276"/>
        <source>Reset the information feedback box.</source>
        <translation>Nastavit znovu okno s informacemi.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1291"/>
        <source>Delete &amp;recent files/URLs list</source>
        <translation>Smazat seznam &amp;nedávných souborů/adres</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1294"/>
        <source>Remove the Qt file who keeps the list of the recent files for the window selection.</source>
        <translation>Odstranit soubor Qt, který uchovává seznam nedávných souborů výběrového okna.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1309"/>
        <source>View &amp;information feedback box</source>
        <translation>Zobrazit okno s &amp;informacemi</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1312"/>
        <source>Show or hide the information feedback box.</source>
        <translation>Ukázat nebo skrýt okno s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1324"/>
        <source>&amp;Anchor information feedback box</source>
        <translation>&amp;Ukotvit okno s informacemi</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1327"/>
        <source>Anchor or loose information feedback box.</source>
        <translation>Ukotvit nebo odpojit okno s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1342"/>
        <source>&amp;Keep the window aspect and its position</source>
        <translation>&amp;Zachovat velikost okna a jeho polohu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1345"/>
        <source>Keep in memory the aspect and the position of the window for the next opened.</source>
        <translation>Uchovat v paměti velikost okna a jeho polohu pro jeho příští otevření.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1357"/>
        <source>&amp;Debug mode</source>
        <translation>&amp;Ladicí režim</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1360"/>
        <source>Enable this option for view more informations in feedback box.</source>
        <translation>Povolit tuto volbu pro zobrazení více informací v okně s informační zpětnou vazbou.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1375"/>
        <source>&amp;Use a system tray icon</source>
        <translation>P&amp;oužít ikonu v oznamovací oblasti panelu</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1378"/>
        <source>Display a system tray icon of the software.</source>
        <translation>Zobrazit ikonu programu v oznamovací oblasti panelu.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1383"/>
        <source>Ass&amp;istant</source>
        <translation>&amp;Pomocník</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1392"/>
        <source>&amp;MKVToolNix last version</source>
        <translation>Poslední verze &amp;MKVToolNix</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1395"/>
        <source>Open the download page of MKVToolNix.</source>
        <translation>Otevřít stahovací stránku MKVToolNix.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1407"/>
        <source>&amp;They talk about</source>
        <translation>&amp;Mluví o</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1410"/>
        <source>They talk about MKV Extractor Gui.</source>
        <translation>Mluví o MKV Extractor Gui.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1437"/>
        <source>Use avconv</source>
        <translation>Použít avconv</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1445"/>
        <source>Use ffmpeg</source>
        <translation>Použít ffmpeg</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1422"/>
        <source>A&amp;bout Qtesseract5</source>
        <translation>O p&amp;rogramu Qtesseract5</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1135"/>
        <source>Display information about MKV Extractor Qt5.</source>
        <translation>Zobrazit informace o MKV Extractor Qt5.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="1425"/>
        <source>Display information about Qtesseract5.</source>
        <translation>Zobrazit informace o Qtesseract5.</translation>
    </message>
    <message>
        <location filename="ui_MKVExtractorQt5.ui" line="786"/>
        <source>Press button for pause between 2 jobs or during the SUB to SRT convert.</source>
        <translation>Klepněte pro pozastavení mezi dvěmi úlohami nebo během převodu ze SUB na SRT.</translation>
    </message>
</context>
</TS>
